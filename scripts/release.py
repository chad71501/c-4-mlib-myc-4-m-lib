import sys
import os
import requests
import json
from settings import SETTINGS
from releasenote import RELEASENOTE

def release():
    tag = SETTINGS['version']
    pid = SETTINGS['gitlab_pid']
    pname = SETTINGS['pname']

    # 創建release及tag
    r = requests.post(
        url='https://gitlab.com/api/v4/projects/{}/releases'.format(pid),
        headers={
            'Private-Token': SETTINGS['PAT']
        },
        params={
            'name': tag,
            'tag_name': tag,
            'ref': 'master',
            'description': RELEASENOTE
        }
    )
    print('創建release及tag')
    print(json.dumps(r.json(),indent=2))
    print('')

    # 上傳檔案並取得檔案下載連結
    filenames = ['dist/' + f for f in os.listdir('dist')]
    for filename in filenames:
        r = requests.post(
            url='https://gitlab.com/api/v4/projects/{}/uploads'.format(pid),
            headers={
                'Private-Token': SETTINGS['PAT'],
            },
            files={
                'file': open(filename, 'rb')
            }
        )

        name = r.json()['alt']
        url = "https://gitlab.com/MVMC-lab/c4mlib/{}/{}".format(pname, r.json()['url'])
        
        # 將檔案下載連結加入release的assets
        r2 = requests.post(
            url='https://gitlab.com/api/v4/projects/{}/releases/{}/assets/links'.format(
                pid,
                tag
            ),
            headers={
                'Private-Token': SETTINGS['PAT']
            },
            params={
                'name': name,
                'url': url
            }
        )
        print('將檔案上傳並加入assets')
        print(json.dumps(r2.json(),indent=2))
        print('')


def run():
    release()

if __name__ == "__main__":
    run()

import configparser
import ast
import os

__ALL__ = ['SETTINGS']

def read_compiler(cfg, compiler):
    c = dict()
    c['name'] = cfg.get(compiler, 'name')
    c['path'] = cfg.get(compiler, 'path')
    return c

def read_device(cfg, device):
    d = dict()
    d['name'] = cfg.get(device, 'name')
    d['platform'] = cfg.get(device, 'platform')
    d['mcu']  = cfg.get(device, 'mcu')
    d['fcpu'] = cfg.get(device, 'fcpu')
    return d

def cicd_variables():
    v = dict()
    if os.getenv('CI') != None:
        v['PAT']        = os.environ['GITLAB_PAT']
        v['gitlab_pid'] = os.environ['CI_PROJECT_ID']
        v['pname']      = os.environ['CI_PROJECT_NAME']
    return v

def read_modules(cfg):
    m = dict()
    PACK = ['C4MBios','ServiceProvi','SysOperat']
    for pack in PACK: 
        m[pack] = ast.literal_eval(cfg.get('Modules', pack))
    return m

def parse_sets(file):
    res = dict()
    cfg = configparser.ConfigParser()
    cfg.read(file)
    res['version'] = cfg.get('General', 'version')
    res['library_path'] = cfg.get('General', 'library_path')
    
    res.update(cicd_variables())

    res['modules'] = read_modules(cfg)

    res['devices'] = list()
    for device in ast.literal_eval(cfg.get('General', 'devices')):
        res['devices'] += [read_device(cfg, device)]

    res['avr_compilers'] = list()
    for compiler in ast.literal_eval(cfg.get('General', 'avr_compilers')):
        res['avr_compilers'] += [read_compiler(cfg, compiler)]

    res['arm_compilers'] = list()
    for compiler in ast.literal_eval(cfg.get('General', 'arm_compilers')):
        res['arm_compilers'] += [read_compiler(cfg, compiler)]
    return res

_DEF_SETFILE = 'scripts/settings.ini'
_USER_SETFILE = 'scripts/settings.user.ini'

if os.path.isfile(_USER_SETFILE):
    SETTINGS = parse_sets(_USER_SETFILE)
elif os.path.isfile(_DEF_SETFILE):
    SETTINGS = parse_sets(_DEF_SETFILE)
else:
    raise("can't find setting file")

def run():
    print(SETTINGS['modules'])
    pass
    
if __name__ == "__main__":
    run()

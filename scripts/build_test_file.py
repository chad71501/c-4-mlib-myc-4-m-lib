import os
from settings import SETTINGS

def CheC4mTest(sets, device, compiler):
    libpath = sets['library_path']
    cmd = 'make test -s -C '+ libpath + ' '
    cmd += 'COMPILER_PATH={} MCU={} FCPU={}'.format(
        compiler['path'],
        device['mcu'],
        device['fcpu']
    )
    print('  start run command:' + cmd)
    os.system(cmd)

def run():
    for device in SETTINGS['devices']:
        for compiler in SETTINGS['avr_compilers']:
            CheC4mTest(SETTINGS, device, compiler)
            
    
if __name__ == "__main__":
    run()

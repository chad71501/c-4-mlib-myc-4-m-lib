import sys
import os
import requests
from settings import SETTINGS


def get_tags():
    url = 'https://gitlab.com/api/v4/projects/{pid}/repository/tags'.format(
        pid = SETTINGS['gitlab_pid']
    )
    r = requests.get(
        url=url,
        headers={
            'Private-Token': SETTINGS['PAT']
        }
    )
    j = r.json()
    return [t['name'] for t in j]


def test_tag_exist():
    tag = SETTINGS['version']

    exist_tags = get_tags()

    for exist_tag in exist_tags:
        if tag == exist_tag:
            print(f'ERROR: tag {tag} is already exists')
            sys.exit(1)

    lastest_tag = exist_tags[0]
    print(f'lastest tag is {lastest_tag}')
    print(f'current tag is {tag}')


def run():
    test_tag_exist()


if __name__ == "__main__":
    run()

/**
 * @file twi.h
 * @author Yuchen
 * @date 2019.10.01
 * @brief 放置TWI通訊封包通用函式及Macro
 *
 */

#ifndef C4MLIB_ASATWI_TWI_H
#define C4MLIB_ASATWI_TWI_H

#include <avr/io.h>
#include <util/delay.h>

/**
 * @defgroup asatwi_macro asatwi macros
 * @defgroup asatwi_func  asatwi functions
 */

/*TWI 狀態列表 */
/*status codes check*/
#define TWI_STATUS ((TWSR) & (0xF8))  ///< @ingroup asatwi_macro
//-----------------------------------------------------------------------------
/*status codes for SLA*/
#define SLA_type_error 0xD0  ///< @ingroup asatwi_macro
//-----------------------------------------------------------------------------
/*Status codes for start and restart condition*/
#define TWI_START 0x08      ///< @ingroup asatwi_macro
#define TWI_REP_START 0x10  ///< @ingroup asatwi_macro
//-----------------------------------------------------------------------------
/*Status codes for master transmitter mode*/
#define TWI_MT_SLA_ACK 0x18    ///< @ingroup asatwi_macro
#define TWI_MT_SLA_NACK 0x20   ///< @ingroup asatwi_macro
#define TWI_MT_DATA_ACK 0x28   ///< @ingroup asatwi_macro
#define TWI_MT_DATA_NACK 0x30  ///< @ingroup asatwi_macro
#define TWI_MT_ARB_LOST 0x38   ///< @ingroup asatwi_macro
//-----------------------------------------------------------------------------
/*Status codes for master receiver mode*/
#define TWI_MR_SLA_ACK 0x40    ///< @ingroup asatwi_macro
#define TWI_MR_SLA_NACK 0x48   ///< @ingroup asatwi_macro
#define TWI_MR_DATA_ACK 0x50   ///< @ingroup asatwi_macro
#define TWI_MR_DATA_NACK 0x58  ///< @ingroup asatwi_macro
#define TWI_MR_ARB_LOST 0x38   ///< @ingroup asatwi_macro
//-----------------------------------------------------------------------------
/*Status codes for Slave transmitter mode*/
#define TWI_ST_SLA_ACK 0xA8           ///< @ingroup asatwi_macro
#define TWI_ST_SLA_Arb_lose_ACK 0xB0  ///< @ingroup asatwi_macro
#define TWI_ST_DATA_ACK 0xB8          ///< @ingroup asatwi_macro
#define TWI_ST_DATA_NACK 0xC0         ///< @ingroup asatwi_macro
#define TWI_ST_Data_last 0xC8         ///< @ingroup asatwi_macro
//-----------------------------------------------------------------------------
/*Status codes for Slave receiver mode*/
#define TWI_SR_SLA_ACK 0x60             ///< @ingroup asatwi_macro
#define TWI_SR_SLA_Arb_lose_ACK 0x68    ///< @ingroup asatwi_macro
#define TWI_SR_SLA_Gen_ACK 0x70         ///< @ingroup asatwi_macro
#define TWI_SR_SLA_ArbLos_Gen_ACK 0x78  ///< @ingroup asatwi_macro
#define TWI_SR_DATA_ACK 0x80            ///< @ingroup asatwi_macro
#define TWI_SR_DATA_NACK 0x88           ///< @ingroup asatwi_macro
#define TWI_SR_DATA_Gen_ACK 0x90        ///< @ingroup asatwi_macro
#define TWI_SR_DATA_Gen_NACK 0x98       ///< @ingroup asatwi_macro
#define TWI_SR_DATA_STO 0xA0            ///< @ingroup asatwi_macro
/*ACK status*/
#define USE_ACK 0x01   ///< @ingroup asatwi_macro
#define USE_NACK 0x00  ///< @ingroup asatwi_macro
//-----------------------------------------------------------------------------

/* Public Section Start */
#define TIMEOUTSETTING 500000  ///< TWI逾時設定   @ingroup asatwi_macro
#define CF_BIT 3               ///< TWI Contral Flag bit數設定 @ingroup asatwi_macro
#define CF_MASK 0xE0           ///< TWI Contral Flag 遮罩 @ingroup asatwi_macro
#define TWAR_MASK 0xFE         ///< TWI TWAR 遮罩 @ingroup asatwi_macro
#define TIMEOUT_FLAG 0X01      ///< TWI 逾時期標 @ingroup asatwi_macro
#define REG_MAX_COUNT 20       ///< TWI 暫存計數器最大值 @ingroup asatwi_macro
#define BUFF_MAX_SZ 32         ///< TWI BUFFER最大值 @ingroup asatwi_macro

/**
 * @brief TWI Hardware 傳輸
 *
 * @ingroup     asatwi_func
 * @param reg   傳輸資料。
 * @return  uint8_t 錯誤代碼：
 *                   - 1：Timeout
 *                   - 其餘編號：參考TWI狀態列表。
 *
 * 將欲傳送資料存入TWI Data Register (TWDR) 中，並對TWI Configer Register
 * (TWCR)中的TWINT和TWEN寫入。
 */
uint8_t TWI_Reg_tram(uint8_t reg);

/**
 * @brief TWI Hardware 接收 ACK回覆
 *
 * @ingroup     asatwi_func
 * @param data  指標指向資料。
 * @return  uint8_t 錯誤代碼：
 *                   - 1：Timeout
 *                   - 其餘編號：參考TWI狀態列表。
 *
 * 對TWI Configer Register (TWCR)中的TWINT、TWEN和TWEA寫入，並將TWI Data
 * Register (TWDR) 接收到的資料存入指定位址指標中。
 */
uint8_t TWI_Reg_rec(uint8_t *data);
/**
 * @brief TWI Hardware 接收 NACK回覆
 *
 * @ingroup     asatwi_func
 * @param data  指標指向資料。
 * @return  uint8_t 錯誤代碼：
 *                   - 1：Timeout
 *                   - 其餘編號：參考TWI狀態列表。
 *
 * 對TWI Configer Register (TWCR)中的TWINT和TWEN寫入，並將TWI Data
 * Register (TWDR) 接收到的資料存入指定位址指標中。
 */
uint8_t TWI_Reg_recNack(uint8_t *data);
/**
 * @brief TWI Hardware Acknowledge
 *
 * @ingroup asatwi_func
 * @param ack_p Acknowledge Flag
 *
 * 決定TWI Configer Register 中的Acknowledge bit(TWEA)是否要enable。
 */
void TWICom_ACKCom(uint8_t ack_p);
/**
 * @brief TWI Hardware Stop
 *
 * @ingroup asatwi_func
 * @param stop_p Stop Flag
 * 
 * 決定TWI Configer Register 中的Stop bit(TWSTO)是否要enable。
 */
void TWICom_Stop(uint8_t stop_p);
/**
 * @brief TWI Hardware Start
 *
 * @ingroup asatwi_func
 * @param _start start Flag
 *
 * 決定TWI Configer Register 中的Start bit(TWSTA)是否要enable。
 */
uint8_t TWICom_Start(uint8_t _start);
/* Public Section End */

#endif  // C4MLIB_ASATWI_TWI_H

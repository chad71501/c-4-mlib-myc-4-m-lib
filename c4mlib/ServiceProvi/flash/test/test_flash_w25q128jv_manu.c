/**
 * @file test_flash_w25q128jv_manu.c
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2020.10.07
 * @brief 使用SPI Master記憶傳輸和接收驅動函式測試w25q128jv flash IC
 * 基本ID顯示更功能。
 *
 * # 追記:
 *      1. flash 預定不佔用 ASAID，故原先 spi 內測試移至此進行
 *      2. 原先 Mem 相關函式仍然保留有 ASAID input，但並未使用，預計討論後移除
 *      3. SPIM_Inst 原先實例化初始方式預計更改為使用 SPI_HW_LAY
 *      4. 雖不使用 SPIM_Inst，但該功能仍存在，並未關閉
 *      5. CS 電位函式抽象化
 * 
 *      原先實例化作法，目前停用中...
 *      SPIM_Inst.init();
 *      DDRB |= (0x10);
 *
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#define SPI_MODE        6
#define ASAID           8
#define SPI_DUMMY       0x77

#define write_command   0x90
#define read_command    0x03
#define read_id_command 0x90

uint8_t mem_addr[3] = {0, 0, 0};
uint8_t manu_cont[2] = {0, 0};

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void) {
    // Setup
    C4M_STDIO_init();

    DDRB |= 0x07;
    SPI_init();

    Extern_CS_disable(ASAID);
    // PORTB |= (0x10);

    printf("Start test flash Manufacturer ID reading...\n");

    while(true) {
        printf("=================\n");

        // First byte dummy data
        

        _delay_us(20);

        SPIM_Mem_rec(SPI_MODE, ASAID, write_command, 3, mem_addr,
                     sizeof(manu_cont), manu_cont);

        for (uint8_t i = 0; i < 2; i++) {
            printf("manu_cont=>%x\n", manu_cont[i]);
        }

        _delay_ms(2000);
    }

}

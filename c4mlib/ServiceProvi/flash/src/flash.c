/**
 * @file flash.c
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2020.07.29
 * @brief flash 記憶體相關實作
 *
 */
#include "c4mlib/ServiceProvi/flash/src/flash.h"

#include "c4mlib/C4MBios/debug/src/debug.h"

static LinkStr_t Link_str;
static uint8_t ASAID_FLASH = 8;

uint8_t FlashFormat(DiscStr_t* DStr_p, uint8_t CardID) {
    uint32_t Memaddr = 0;

    DiscStr_Init(DStr_p, CardID);
    ASAID_FLASH = DStr_p->CardId;

    // Flash chip format
    SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, WRITE_ENABLE, 0, 0, 0, 0);
    Flash_wait_to_complete();
    SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, ENTIRE_ERASE, 0, 0, 0, 0);
    Flash_wait_to_complete();

    // Write FlashDisc to flash memory
    Memaddr          = DISC_INFO;
    DStr_p->FSLLHead = SECTOR(DATA_START, 0);
    DStr_p->FSLLTail = SECTOR(DATA_END, 15);
    Flash_page_write(Memaddr, sizeof(DiscStr_t), DStr_p);

    // Write Sector Info to each sector
    Memaddr = SECTOR(DATA_START, 0);
    for (uint16_t i = 0; i < (DStr_p->DataSectorMax); i++) {
        Link_str.Sector = i;
        if (i == (DStr_p->DataSectorMax) - 2) {
            Link_str.NextLink = SECTOR_END;
        }
        else {
            Link_str.NextLink = Memaddr + SECTOR_SIZE;
        }

        // Write LinkStr into Flash
        Flash_page_write(Memaddr, sizeof(Link_str), &Link_str);

        Memaddr += SECTOR_SIZE;
    }

    return FLASH_FORMAT_SUCCESS;  // TODO: 回傳值待確認
}

uint8_t FlashInfo(DiscStr_t* DStr_p, uint8_t CardID) {
    // 讀取特定記憶體位置，判斷當前記憶體狀態
    uint8_t Recv_tmp[10] = {0};
    ASAID_FLASH          = CardID;
    DStr_p->CardId       = CardID;
    Flash_read_data(DISC_INFO, sizeof(DiscStr_t), DStr_p);
    Flash_manufacture_code(Recv_tmp);
    if (Recv_tmp[0] != 0xEF) {
        if (Recv_tmp[1] != 0x17) {
            return FLASH_SPI_FAIL_CODE;
        }
    }

    // Scan DISC_INFO
    Flash_read_data(DISC_INFO, 4, Recv_tmp);
    if ((Recv_tmp[0] != 0xFF) && (Recv_tmp[1] != 0xFF)) {
        if ((Recv_tmp[2] != 0xFF) && (Recv_tmp[3] != 0xFF)) {
            // 成功讀取 且已格式化過 (可能有寫入紀錄)
            return FLASH_FORMATED_CODE;
        }
    }
    else if ((Recv_tmp[0] == 0xFF) && (Recv_tmp[1] == 0xFF)) {
        if ((Recv_tmp[1] == 0xFF) && (Recv_tmp[2] == 0xFF)) {
            return FLASH_BLANK_CODE;
        }
    }

    return FLASH_DATA_ERROR_CODE;
}

uint8_t FileOpen(DiscStr_t* DStr_p, FileStr_t* FStr_p, uint8_t FileId,
                 char* Name_p) {
    uint32_t Memaddr = 0;
    // Read DiscStr_t from flash
    Memaddr = DISC_INFO;
    Flash_read_data(Memaddr, sizeof(DiscStr_t), DStr_p);

    // File Name align to flash
    for (uint8_t j = 0; j < FlASH_NAME_LENGTH; j++) {
        if (*(Name_p + j) == 0) {
            *(Name_p + j) = 0xFF;
        }
    }

    // Open By File name
    Memaddr = FILE_INFO;
    if (FileId == 0) {
        // Search for Match Name
        uint8_t namechk;
        for (uint16_t i = 0; i < (DStr_p->FileTotal); i++) {
            // Read FileStr from flash
            Flash_read_data(Memaddr, sizeof(FileStr_t), FStr_p);

            Memaddr += sizeof(FileStr_t);
            namechk = 0;
            for (uint8_t j = 0; j < FlASH_NAME_LENGTH; j++) {
                if ((FStr_p->FileName)[j] != *(Name_p + j)) {
                    namechk++;
                    break;
                }
            }
            if (namechk == 0) {
                // NOTE: 找到相對應Name，成功開檔
                return FLASH_SUCCESS_OPEN_FILE_BYNAME;
            }
        }

        // No match, establish a new file
        DStr_p->Writen = 1;
        FileStr_Init(FStr_p);
        DStr_p->FileTotal += 1;
        if (DStr_p->FileTotal <= DStr_p->FileMax) {
            // Sector 新增 & FSLL位移，釋放空間(x1)給file使用
            FStr_p->FLLHead = DStr_p->FSLLHead;
            FStr_p->FLLTail = FStr_p->FLLHead;
            DStr_p->FSLLHead += SECTOR_SIZE;

            // Read FLL Head LinkStr
            Memaddr = FStr_p->FLLHead;
            Flash_read_data(Memaddr, sizeof(LinkStr_t), &Link_str);
            Link_str.NextLink = SECTOR_END;

            for (uint8_t i = 0; i < FlASH_NAME_LENGTH; i++) {
                FStr_p->FileName[i] = *(Name_p + i);
            }
            // Write FileStr into Flash
            Flash_page_write(Memaddr, sizeof(FileStr_t), FStr_p);
            Memaddr = FILE_INFO + ((DStr_p->FileTotal - 1) * sizeof(FileStr_t));
            Flash_page_write(Memaddr, sizeof(FileStr_t), FStr_p);

            // 清空檔案 sector
            Flash_clear_sector(FStr_p->FLLHead);

            // 改寫對應 file sector next 為終點值
            Flash_page_write(FStr_p->FLLHead, sizeof(LinkStr_t), &Link_str);

            return FLASH_NEW_FILE_SUCCESS_BYNAME;
        }
        else {
            // NOTE: 檔案數量達到上限，無法新增檔案
            return FLASH_FILENUM_EXCEED_BYNAME;
        }
    }
    else {  // Open By File ID
        if (FileId <= (DStr_p->FileTotal) + 1) {
            // ID 符合條件才給新建
            if (FileId == ((DStr_p->FileTotal) + 1)) {
                DStr_p->Writen = 1;
                FileStr_Init(FStr_p);
                DStr_p->FileTotal += 1;
                if (DStr_p->FileTotal <= DStr_p->FileMax) {
                    // Sector 新增 & FSLL位移，釋放空間(x1)給file使用
                    FStr_p->FLLHead = DStr_p->FSLLHead;
                    FStr_p->FLLTail = FStr_p->FLLHead;
                    DStr_p->FSLLHead += SECTOR_SIZE;

                    // Read FLL Head LinkStr
                    Memaddr = FStr_p->FLLHead;
                    Flash_read_data(Memaddr, sizeof(LinkStr_t), &Link_str);
                    Link_str.NextLink = SECTOR_END;

                    for (uint8_t i = 0; i < FlASH_NAME_LENGTH; i++) {
                        FStr_p->FileName[i] = *(Name_p + i);
                    }
                    // Write FileStr into Flash
                    Memaddr = FILE_INFO + ((FileId - 1) * sizeof(FileStr_t));
                    Flash_page_write(Memaddr, sizeof(FileStr_t), FStr_p);

                    // 清空檔案 sector
                    Flash_clear_sector(FStr_p->FLLHead);

                    // 改寫對應 file sector next 為終點值
                    Flash_page_write(FStr_p->FLLHead, sizeof(LinkStr_t),
                                     &Link_str);

                    return FLASH_NEW_FILE_SUCCESS;
                }
                else {
                    // NOTE: 檔案數量達到上限，無法新增檔案
                    return FLASH_FILENUM_EXCEED;
                }
            }
            else {
                // Read from ID address
                Memaddr = FILE_INFO + ((FileId - 1) * sizeof(FileStr_t));
                Flash_read_data(Memaddr, sizeof(FileStr_t), FStr_p);
                // 偵測非法FileName
                if (FStr_p->FileName[0] == 0xFF) {
                    return FLASH_ILLEGAL_FILE_NAME;
                }

                return FLASH_SUCCESS_OPEN_FILE;
            }
        }
        else {
            // NOTE: 非法ID
            return FLASH_INVALID_FILEID;
        }
    }

    // NOTE: 例外情況
    return FLASH_UNEXPECT_EVENT;
}

uint8_t FileClose(DiscStr_t* DStr_p, FileStr_t* FStr_p) {
    uint32_t Memaddr      = 0;
    uint32_t Back_Memaddr = 0;
    FileStr_t tmp_Fstr;
    uint8_t Match_count = 0;
    uint8_t Match_found = 0;
    if (DStr_p->Writen == 0)  // File is not written
    {
        // DStr_p -> RShift = 0;
        // DStr_p -> RBytes = 0;
        return FLASH_FILE_NO_CHANGE;
    }
    else  // File has been written
    {
        DStr_p->Writen = 0;
        Memaddr        = BLOCK(251);
        // 寫入新資料至 Backup block
        Flash_page_write(Memaddr, sizeof(DiscStr_t), DStr_p);

        Memaddr      = FILE_INFO;
        Back_Memaddr = BLOCK(251) + DISC_STR_SPACE;
        for (uint16_t i = 0; i < DStr_p->FileTotal; i++) {
            Flash_read_data(Memaddr, sizeof(FileStr_t), &tmp_Fstr);
            Match_count = 0;
            for (uint8_t i = 0; i < FlASH_NAME_LENGTH; i++) {
                if (FStr_p->FileName[i] == tmp_Fstr.FileName[i] &&
                    Match_found == 0) {
                    Match_count += 1;
                }
            }

            if (Match_count == FlASH_NAME_LENGTH) {
                Flash_page_write(Back_Memaddr, sizeof(FileStr_t), FStr_p);
                Match_found = 1;
            }
            else {
                Flash_page_write(Back_Memaddr, sizeof(FileStr_t), &tmp_Fstr);
            }
            Memaddr += sizeof(FileStr_t);
            Back_Memaddr += sizeof(FileStr_t);
        }

        // 取代原 sys block 為 Backup block 內容
        Memaddr = DISC_INFO;
        Flash_clear_block(Memaddr);
        Flash_page_write(Memaddr, sizeof(DiscStr_t), DStr_p);

        Memaddr      = FILE_INFO;
        Back_Memaddr = BLOCK(251) + DISC_STR_SPACE;

        for (uint16_t i = 0; i < DStr_p->FileTotal; i++) {
            Flash_read_data(Back_Memaddr, sizeof(FileStr_t), &tmp_Fstr);
            Flash_page_write(Memaddr, sizeof(FileStr_t), &tmp_Fstr);
            Memaddr += sizeof(FileStr_t);
            Back_Memaddr += sizeof(FileStr_t);
        }

        // NOTE: 成功關檔，清除tmp block
        Flash_clear_block(BLOCK(251));
        return FLASH_FILE_CLOSE_SUCCESS;
    }
    // 例外情況
    return FLASH_UNEXPECT_EVENT;
}

uint8_t FileDelete(DiscStr_t* DStr_p, uint8_t FileID, char* Name_p) {
    uint32_t Memaddr  = 0;
    uint32_t Back     = 0;
    uint8_t Del_Count = 0;
    uint8_t Del_Found = 0;
    // Read DiscStr_t from flash
    Memaddr = DISC_INFO;
    Flash_read_data(Memaddr, sizeof(DiscStr_t), DStr_p);

    // File Name align to flash
    for (uint8_t j = 0; j < FlASH_NAME_LENGTH; j++) {
        if (*(Name_p + j) == 0) {
            *(Name_p + j) = 0xFF;
        }
    }

    // Search By File name
    Memaddr = FILE_INFO;
    if (FileID == 0) {
        FileStr_t tmpfile;
        Back    = BLOCK(252) + DISC_STR_SPACE;
        Memaddr = FILE_INFO;
        for (uint16_t DelID = 1; DelID <= DStr_p->FileTotal; DelID++) {
            Flash_read_data(Memaddr, sizeof(FileStr_t), &tmpfile);
            Del_Count = 0;
            if (Del_Found == 0) {
                for (uint8_t i = 0; i < FlASH_NAME_LENGTH; i++) {
                    if (*(Name_p + i) == tmpfile.FileName[i]) {
                        Del_Count += 1;
                    }
                }
            }

            if (Del_Count == FlASH_NAME_LENGTH) {
                // Memaddr += sizeof(FileStr_t);
                DStr_p->FileTotal -= 1;
                Del_Found = 1;

                // 更新 Tail Link
                Flash_read_data(DStr_p->FSLLTail, sizeof(LinkStr_t), &Link_str);
                Link_str.NextLink = tmpfile.FLLHead;
                Flash_clear_sector(DStr_p->FSLLTail);
                Flash_page_write(DStr_p->FSLLTail, sizeof(LinkStr_t),
                                 &Link_str);

                DStr_p->FSLLTail = tmpfile.FLLTail;
            }
            else {
                Flash_page_write(Back, sizeof(FileStr_t), &tmpfile);
                Back += sizeof(FileStr_t);
            }

            Memaddr += sizeof(FileStr_t);
        }

        if (!Del_Found) {
            Flash_clear_block(BLOCK(252));
            return FLASH_DELETE_NOT_FOUND_BY_NAME;
        }

        // 更新 SYS BLOCK
        Back = BLOCK(252);
        Flash_page_write(Back, sizeof(DiscStr_t), DStr_p);

        // Rewrite SysBlock with tmp block
        Flash_clear_block(BLOCK(0));
        Flash_page_write(BLOCK(0), sizeof(DiscStr_t), DStr_p);
        Back    = BLOCK(252) + DISC_STR_SPACE;
        Memaddr = BLOCK(0) + DISC_STR_SPACE;
        for (uint16_t DelID = 1; DelID <= DStr_p->FileTotal; DelID++) {
            Flash_read_data(Back, sizeof(FileStr_t), &tmpfile);
            Flash_page_write(Memaddr, sizeof(FileStr_t), &tmpfile);
        }

        return FLASH_DELETE_SUCCESS_BY_NAME;
    }
    else {  // Delete By File ID
        if (FileID <= (DStr_p->FileTotal)) {
            DStr_p->FileTotal -= 1;

            FileStr_t tmpfile;
            Back    = BLOCK(252) + DISC_STR_SPACE;
            Memaddr = FILE_INFO;
            for (uint16_t DelID = 1; DelID <= DStr_p->FileTotal; DelID++) {
                Flash_read_data(Memaddr, sizeof(FileStr_t), &tmpfile);
                if (DelID == FileID) {
                    Del_Found = 1;

                    // 更新 Tail Link
                    Flash_read_data(DStr_p->FSLLTail, sizeof(LinkStr_t),
                                    &Link_str);
                    Link_str.NextLink = tmpfile.FLLHead;
                    Flash_clear_sector(DStr_p->FSLLTail);
                    Flash_page_write(DStr_p->FSLLTail, sizeof(LinkStr_t),
                                     &Link_str);

                    DStr_p->FSLLTail = tmpfile.FLLTail;
                }
                else {
                    Flash_page_write(Back, sizeof(FileStr_t), &tmpfile);
                    Back += sizeof(FileStr_t);
                }
                Memaddr += sizeof(FileStr_t);
            }

            if (!Del_Found) {
                Flash_clear_block(BLOCK(252));
                return FLASH_DELETE_NOT_FOUND;
            }

            // 更新 SYS BLOCK
            Back = BLOCK(252);
            Flash_page_write(Back, sizeof(DiscStr_t), DStr_p);

            // Rewrite SysBlock with tmp block
            Flash_clear_block(BLOCK(0));
            Flash_page_write(BLOCK(0), sizeof(DiscStr_t), DStr_p);
            Back    = BLOCK(252) + DISC_STR_SPACE;
            Memaddr = BLOCK(0) + DISC_STR_SPACE;
            for (uint16_t DelID = 1; DelID <= DStr_p->FileTotal; DelID++) {
                Flash_read_data(Back, sizeof(FileStr_t), &tmpfile);
                Flash_page_write(Memaddr, sizeof(FileStr_t), &tmpfile);
            }

            return FLASH_DELETE_SUCCESS;
        }
        else {
            // NOTE: 非法ID
            return FLASH_INVALID_FILEID;
        }
    }

    // NOTE: 例外情況
    return FLASH_UNEXPECT_EVENT;
}

uint8_t FileWrite(DiscStr_t* DStr_p, FileStr_t* FStr_p, uint8_t Bytes,
                  void* Data_p) {
    uint32_t Memaddr;
    uint32_t Memaddr_tmp;
    uint8_t tmp_data = 0;
    // 寫入大小超出判斷
    if ((FStr_p->WAddr + Bytes) > SECTOR_SIZE) {
        // NOTE: 需改寫 Link
        Memaddr = FStr_p->FLLTail;
        Flash_read_data(Memaddr, sizeof(LinkStr_t), &Link_str);

        // 前 Sector 填滿
        Link_str.NextLink = DStr_p->FSLLHead;
        Flash_page_write(BLOCK(251), sizeof(LinkStr_t), &Link_str);
        Memaddr     = FStr_p->FLLTail + sizeof(LinkStr_t);
        Memaddr_tmp = BLOCK(251) + sizeof(LinkStr_t);
        for (uint16_t i = 0; i < (FStr_p->WAddr); i += 8) {
            Flash_read_data(Memaddr, sizeof(uint8_t), &tmp_data);
            Flash_page_write(Memaddr, sizeof(uint8_t), &tmp_data);
            Memaddr += sizeof(uint8_t);
            Memaddr_tmp += sizeof(uint8_t);
        }
        for (uint16_t i = 0;
             i < (SECTOR_SIZE - (FStr_p->WAddr) - sizeof(LinkStr_t)); i += 8) {
            Flash_page_write(Memaddr_tmp, sizeof(uint8_t), (Data_p + i));
        }
        Flash_clear_sector(FStr_p->FLLTail);
        Flash_sector_copy(BLOCK(251), FStr_p->FLLTail);

        // 各Head / Tail 更新
        FStr_p->FLLTail   = DStr_p->FSLLHead;
        Link_str.NextLink = SECTOR_END;
        DStr_p->FSLLHead += SECTOR_SIZE;
        Flash_clear_sector(FStr_p->FLLTail);

        Memaddr = FStr_p->FLLTail;
        Flash_page_write(Memaddr, sizeof(LinkStr_t), &Link_str);
        Memaddr += sizeof(LinkStr_t);
        for (uint16_t i = 0; i < (sizeof(Data_p) - tmp_data); i += 8) {
            Flash_page_write(Memaddr, sizeof(uint8_t), (Data_p + tmp_data));
        }
        // TODO: 測試跨sector Waddr 有無問題
        FStr_p->WAddr += Bytes;
        FStr_p->FileSize += Bytes;
        DStr_p->Writen = 1;

        return FLASH_WRITE_CROSS_SUCCESS;
    }
    else {
        // NOTE: 可直接寫入新資料 (首次寫入)
        Memaddr = FStr_p->FLLTail + sizeof(LinkStr_t) + FStr_p->WAddr;
        FStr_p->FileSize += Bytes;
        FStr_p->WAddr += Bytes;
        DStr_p->Writen = 1;
        Flash_page_write(Memaddr, Bytes, Data_p);
        return FLASH_WRITE_NO_CROSS_SUCCESS;
    }
}

uint8_t FileRead(DiscStr_t* DStr_p, FileStr_t* FStr_p, uint16_t Bytes,
                 void* Data_p) {
    uint32_t Memaddr;
    uint32_t Dataoffset = 0;

    if (Bytes > FStr_p->FileSize) {
        // NOTE: 讀取超出範圍
        return FLASH_OVER_READ;
    }
    else {
        // NOTE: Reading 跨 sector
        if (Bytes > SECTOR_SIZE) {
            Memaddr = FStr_p->FLLHead;
            // read Link
            Flash_read_data(Memaddr, sizeof(LinkStr_t), &Link_str);
            // read content
            Flash_read_data(Memaddr + sizeof(LinkStr_t),
                            SECTOR_SIZE - sizeof(LinkStr_t), Data_p);
            Dataoffset += SECTOR_SIZE;

            for (uint8_t i = 0; i < (Bytes / SECTOR_SIZE); i++) {
                Memaddr = Link_str.NextLink;
                // read Link
                Flash_read_data(Memaddr, sizeof(LinkStr_t), &Link_str);
                // read content
                if (i == (Bytes / SECTOR_SIZE) - 1) {
                    Flash_read_data(Memaddr + sizeof(LinkStr_t),
                                    (Bytes % SECTOR_SIZE),
                                    (Data_p + Dataoffset));
                }
                else {
                    Flash_read_data(Memaddr + sizeof(LinkStr_t),
                                    SECTOR_SIZE - sizeof(LinkStr_t),
                                    (Data_p + Dataoffset));
                }

                Dataoffset += SECTOR_SIZE;
            }
            return FLASH_READ_L_SECROR_FINISH;
        }
        else {
            Flash_read_data(FStr_p->FLLHead + sizeof(Link_str), Bytes, Data_p);
            return FLASH_READ_S_SECROR_FINISH;
        }
    }
}

void Flash_wait_to_complete(void) {
    static uint8_t status = 0;
    while (1) {
        SPIM_Mem_rec(SPI_MODE_FLASH, ASAID_FLASH, READ_STATUS, 0, 0, 1,
                     &status);
        if ((status & 0x01) == 0) {
            DEBUG_INFO("--Exit_BUSY_STATE!!--\n");
            break;
        }
    }
}

void Flash_sector_copy(uint32_t from, uint32_t to) {
    // Copy A content to B sector
    static uint8_t tmp[8] = {0};

    // 1. Clear B sector
    SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, SECTOR_ERASE, 3, &to, 0, 0);
    Flash_wait_to_complete();

    // 2. Copy A to B sector
    for (uint16_t i = 0; i < (SECTOR_SIZE / sizeof(tmp)); i++) {
        from = i * 8;
        SPIM_Mem_rec(SPI_MODE_FLASH, ASAID_FLASH, READ_DATA, 3, &from,
                     sizeof(tmp), tmp);
        Flash_wait_to_complete();
        to = i * 8;
        SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, PAGE_PROGRAM, 3, &to,
                     sizeof(tmp), tmp);
        Flash_wait_to_complete();
    }
}

void Flash_page_write(uint32_t Addr, uint16_t Bytes, void* Data_write) {
    SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, WRITE_ENABLE, 0, 0, 0, 0);
    Flash_wait_to_complete();
    SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, PAGE_PROGRAM, 3, &Addr, Bytes,
                 Data_write);
    Flash_wait_to_complete();
}

void Flash_read_data(uint32_t Addr, uint16_t Bytes, void* Recv_data) {
    SPIM_Mem_rec(SPI_MODE_FLASH, ASAID_FLASH, READ_DATA, 3, &Addr, Bytes,
                 Recv_data);
    Flash_wait_to_complete();
}

void Flash_clear_sector(uint32_t Sector_addr) {
    SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, WRITE_ENABLE, 0, 0, 0, 0);
    Flash_wait_to_complete();
    SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, SECTOR_ERASE, 3, &Sector_addr, 0,
                 0);
    Flash_wait_to_complete();
}

void Flash_clear_block(uint32_t Block_addr) {
    SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, WRITE_ENABLE, 0, 0, 0, 0);
    Flash_wait_to_complete();
    SPIM_Mem_trm(SPI_MODE_FLASH, ASAID_FLASH, BLOCK_ERASE, 3, &Block_addr, 0,
                 0);
    Flash_wait_to_complete();
}

void Flash_manufacture_code(uint8_t* Recv_code) {
    uint32_t mem_addr = 0;
    SPIM_Mem_rec(SPI_MODE_FLASH, ASAID_FLASH, READ_MANU, 3, &mem_addr, 2,
                 Recv_code);
}

void FileStr_blank(FileStr_t* FStr_p) {
    uint8_t* a = (uint8_t*)FStr_p;
    for (uint8_t i = 0; i < sizeof(FileStr_t); i++) {
        *(a + i) = 0xFF;
    }
}

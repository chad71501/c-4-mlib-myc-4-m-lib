/**
 * @file asaspi_master.c
 * @author Deng Xiang-Guan
 * @date 2019.10.02
 * @brief 實現ASA SPI
 * Master(主)的驅動函式，提供使用者呼叫已設定ASA單板電腦SPI的工作模式，
 * 並提供多種不同模式通訊協定配合ASA介面卡協定，達成資料送收之目的。
 *
 * NOTE: _delay_ms
 * 在debug的時候要加，因printf會造成時間的延遲，如果不加，會造成通訊失敗。
 */

#include "asaspi_master.h"

#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/C4MBios/asabus/src/remo_reg.h"
#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"

#ifdef USE_C4MLIB_DEBUG
#    define SPI_DELAY(tick)                                                    \
        { _delay_ms(20); }
#else
#    define SPI_DELAY(tick)                                                    \
        for (int i = 0; i < tick; i++) {                                       \
            __asm__ __volatile__("nop");                                       \
        }
#endif

uint8_t ASA_SPIM_trm(uint8_t mode, uint8_t ASAID, uint8_t RegAdd, uint8_t Bytes,
                     void *Data_p, uint16_t WaitTick) {
    uint8_t echo = 0, err = 0, chk = 0;
    switch (mode) {
        case 0:
            chk = SPIM_Inst.spi_swap((RegAdd | 0x80));  // add+bit7H
            DEBUG_INFO("get=%d\n", chk);
            SPI_DELAY(WaitTick);
            SPIM_Inst.spi_swap(*((uint8_t *)Data_p));  // send frist bytes
            DEBUG_INFO("get=%d\n", chk);
            SPI_DELAY(WaitTick);
            for (int i = 1; i < Bytes; i++) {
                echo = SPIM_Inst.spi_swap(
                    *((uint8_t *)Data_p + i));  //送出下一筆&拿回前一筆
                DEBUG_INFO("get=%d\n", echo);
                DEBUG_INFO("old=%d\n", (*((uint8_t *)Data_p + i - 1)));
                if (echo != (*((uint8_t *)Data_p + i - 1)))
                    err = 1;  //檢查前一筆
                SPI_DELAY(WaitTick);
            }
            echo = SPIM_Inst.spi_swap(0);  //喚回最後一筆
            DEBUG_INFO("get=%d\n", echo);
            DEBUG_INFO("old=%d\n", (*((uint8_t *)Data_p + Bytes - 1)));
            SPI_DELAY(WaitTick);
            if (echo != (*((uint8_t *)Data_p + Bytes - 1)))
                err = 1;                    //檢查是否吻合
            chk = SPIM_Inst.spi_swap(err);  //送出是否吻合
            DEBUG_INFO("chk=%d\n", chk);

            return chk;
            break;

        case 1:
            for (int i = 0; i < Bytes; i++) {
                if (i == 0) {
                    SPIM_Inst.spi_swap(RegAdd | (*((uint8_t *)Data_p)));
                    DEBUG_INFO("swap:%d\n", RegAdd | (*((uint8_t *)Data_p)));
                    SPI_DELAY(WaitTick);
                }
                else {
                    SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                    DEBUG_INFO("swap:%d\n", *((uint8_t *)Data_p + i));
                    SPI_DELAY(WaitTick);
                }
            }
            break;

        case 2:
            for (int i = Bytes - 1; i >= 0; i--) {
                if (i == Bytes - 1) {
                    SPIM_Inst.spi_swap(RegAdd |
                                       (*((uint8_t *)Data_p + Bytes - 1)));
                    DEBUG_INFO("swap:%d\n",
                               RegAdd | (*((uint8_t *)Data_p + Bytes - 1)));
                    SPI_DELAY(WaitTick);
                }
                else {
                    SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                    DEBUG_INFO("swap:%d\n", *((uint8_t *)Data_p + i));
                    SPI_DELAY(WaitTick);
                }
            }
            break;

        case 3:
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                DEBUG_INFO("swap:%d\n", *((uint8_t *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 4:
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 5:
            SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 6:
            SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 7:
            SPIM_Inst.spi_swap(RegAdd | 0x80);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 8:
            SPIM_Inst.spi_swap(RegAdd | 0x80);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 9:
            SPIM_Inst.spi_swap((RegAdd << 1) | 1);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 10:
            SPIM_Inst.spi_swap((RegAdd << 1) | 1);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;
        default:
            return HAL_ERROR_MODE_SELECT;
            break;
    }
    return 0;
}

uint8_t ASA_SPIM_rec(uint8_t mode, uint8_t ASAID, uint8_t RegAdd, uint8_t Bytes,
                     void *Data_p, uint16_t WaitTick) {
    uint8_t chk = 0, a = 0;
    switch (mode) {
        case 0:
            SPIM_Inst.spi_swap(RegAdd);
            // slave會檢查所以比較慢，不delay會出錯
            SPI_DELAY(WaitTick);
            *((uint8_t *)Data_p) = SPIM_Inst.spi_swap(0);  //送0喚回第一筆
            a = *((uint8_t *)Data_p);  //把對面給的資料丟回去
            SPI_DELAY(WaitTick);
            for (int i = 1; i < Bytes; i++) {
                *((uint8_t *)Data_p + i) = SPIM_Inst.spi_swap(a);
                a                        = *((uint8_t *)Data_p + i);
                SPI_DELAY(WaitTick);
            }
            SPIM_Inst.spi_swap(a);  //丟回最後一筆
            SPI_DELAY(WaitTick);
            chk = SPIM_Inst.spi_swap(0);  //接收對面的檢查結果
            return chk;
            break;

        case 1:
            return HAL_ERROR_MODE_SELECT;
            break;

        case 2:
            return HAL_ERROR_MODE_SELECT;
            break;

        case 3:
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                *((uint8_t *)Data_p + i) = SPIM_Inst.spi_swap(0);
                DEBUG_INFO("rec %d\n", *((uint8_t *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 4:
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                *((uint8_t *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 5:
            *((uint8_t *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                *((uint8_t *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 6:
            *((uint8_t *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                *((uint8_t *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }

            break;

        case 7:
            SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                *((uint8_t *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 8:
            *((uint8_t *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                *((uint8_t *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 9:
            *((uint8_t *)Data_p + Bytes - 1) =
                SPIM_Inst.spi_swap((RegAdd << 1));
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                *((uint8_t *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 10:
            *((uint8_t *)Data_p + Bytes - 1) =
                SPIM_Inst.spi_swap((RegAdd << 1));
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                *((uint8_t *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }

            break;
        default:
            return HAL_ERROR_MODE_SELECT;
            break;
    }
    return 0;
}

uint8_t ASA_SPIM_frc(uint8_t mode, uint8_t ASAID, uint8_t RegAdd, uint8_t Mask,
                     uint8_t Shift, uint8_t *Data_p, uint16_t WaitTick) {
    if ((mode == 1) || (mode == 2) || (mode == 4) || (mode == 6) ||
        (mode == 8) || (mode == 10))
        return HAL_ERROR_MODE_SELECT;
    uint8_t temp = 0;
    ASA_SPIM_rec(mode, ASAID, RegAdd, 1, &temp, WaitTick);
    REGFGT(&temp, Mask, Shift, Data_p);
    return 0;
}

uint8_t ASA_SPIM_ftm(uint8_t mode, uint8_t ASAID, uint8_t RRegAdd,
                     uint8_t WRegAdd, uint8_t Mask, uint8_t Shift,
                     uint8_t *Data_p, uint16_t WaitTick) {
    if ((mode == 1) || (mode == 2) || (mode == 4) || (mode == 6) ||
        (mode == 8) || (mode == 10))
        return HAL_ERROR_MODE_SELECT;
    uint8_t temp = 0;
    ASA_SPIM_rec(mode, ASAID, RRegAdd, 1, &temp, WaitTick);
    REGFPT(&temp, Mask, Shift, *Data_p);
    ASA_SPIM_trm(mode, ASAID, WRegAdd, 1, &temp, WaitTick);
    return 0;
}

void Extern_CS_enable(uint8_t id) {
    if (id >= 8) {
        switch (id) {
            case 8:
                // Master CS pin is PB4
                ASA_CS_DDR_ID_8 |= (1 << ASA_CS_ID_8);
                ASA_CS_PORT_ID_8 &= ~(1 << ASA_CS_ID_8);
                break;
            case 9:
                // Master CS pin is PB5
                ASA_CS_PORT_ID_9 |= (1 << ASA_CS_ID_9);
                ASA_CS_PORT_ID_9 &= ~(1 << ASA_CS_ID_9);
                break;
            case 10:
                // Master CS pin is PB6
                ASA_CS_PORT_ID_10 |= (1 << ASA_CS_ID_10);
                ASA_CS_PORT_ID_10 &= ~(1 << ASA_CS_ID_10);
                break;
            case 11:
                // Master CS pin is PB7
                ASA_CS_PORT_ID_11 |= (1 << ASA_CS_ID_11);
                ASA_CS_PORT_ID_11 &= ~(1 << ASA_CS_ID_11);
                break;
            default:
                break;
        }
    }
    else {
        ASABUS_ID_set(id);
        ASA_CS_PORT &= ~(1 << ASA_CS);
    }
}

void Extern_CS_disable(uint8_t id) {
    if (id >= 8) {
        switch (id) {
            case 8:
                // Master CS pin is PB4
                ASA_CS_DDR_ID_8 |= (1 << ASA_CS_ID_8);
                ASA_CS_PORT_ID_8 |= (1 << ASA_CS_ID_8);
                break;
            case 9:
                // Master CS pin is PB5
                ASA_CS_DDR_ID_9 |= (1 << ASA_CS_ID_9);
                ASA_CS_PORT_ID_9 |= (1 << ASA_CS_ID_9);
                break;
            case 10:
                // Master CS pin is PB6
                ASA_CS_DDR_ID_10 |= (1 << ASA_CS_ID_10);
                ASA_CS_PORT_ID_10 |= (1 << ASA_CS_ID_10);
                break;
            case 11:
                // Master CS pin is PB7
                ASA_CS_DDR_ID_11 |= (1 << ASA_CS_ID_11);
                ASA_CS_PORT_ID_11 |= (1 << ASA_CS_ID_11);
                break;
            default:
                break;
        }
    }
    else {
        ASA_CS_PORT |= (1 << ASA_CS);
    }
}

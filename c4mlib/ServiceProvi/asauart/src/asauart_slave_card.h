/**
 * @file asauart_slave.h
 * @author s915888
 * @date 2021.05.25
 * @brief 放置UART0通訊函式以及相關marco
 */

#ifndef C4MLIB_ASAUART_ASAUART_SLAVE_H
#define C4MLIB_ASAUART_ASAUART_SLAVE_H

#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"

/**
 * @defgroup asauart0_func
 * @defgroup asauart0_struct
 * @defgroup asauart0_macro
 */
#define ASAUART_CMD_HEADER 0xAA  ///< @ingroup asauart_macro
#define ASAUART_RSP_HEADER 0xAB  ///< @ingroup asauart_macro

/* Public Section Start */
typedef struct {
    volatile uint8_t* UARTDataReg_p;  ///< 硬體相關暫存器以及旗標資訊結構
    uint8_t UARTDataBytes;
    volatile uint8_t* TXEnReg_p;
    uint8_t TXEnMask, TXEnShift;
    volatile uint8_t* RXEnReg_p;
    uint8_t RXEnMask, RXEnShift;
    volatile uint8_t* TXRXSwiReg_p;
    uint8_t TXRXSwiMask, TXRXSwiShift;
    volatile uint8_t* TXIntEnReg_p;
    uint8_t TXIntEnMask, TXIntEnShift;
    volatile uint8_t* RXIntEnReg_p;
    uint8_t RXIntEnMask, RXIntEnShift;
    volatile uint8_t* TXIntClrReg_p;
    uint8_t TXIntClrMask, TXIntClrShift;
    volatile uint8_t* RXIntClrReg_p;
    uint8_t RXIntClrMask, RXIntClrShift;
    volatile uint8_t* TXIntSetReg_p;
    uint8_t TXIntSetMask, TXIntSetShift;
    volatile uint8_t* RXIntSetReg_p;
    uint8_t RXIntSetMask, RXIntSetShift;
    volatile uint8_t* TXIntFReg_p;
    uint8_t TXIntFMask, TXIntFShift;
    volatile uint8_t* RXIntFReg_p;
    uint8_t RXIntFMask, RXIntFShift;
    volatile uint8_t* DataReg_p;
    uint8_t Header;          ///< 封包表頭資料
    uint8_t UID;             ///< slave address
    RemoBFStr_t* RemoBF_p;   ///< slave端暫存器連結remoBF結構
    HWIntStr_t* TOIntStr_p;  ///< 逾時計時函式連結HWInt結構
    uint8_t DDTaskID, TOTaskID, TxTaskID, RxTaskID;  ///< slave函式連結HWInt編號
    uint8_t TrmORec;                     ///< 資料送收判斷變數
    uint8_t RegAdd, Byte, ChkSum, Resp;  ///< slave接收變數
    uint8_t TXState, RXState, TOState;   ///< slave函式狀態變數
    uint8_t TimCount, TimMax;            ///< 逾時計時計數變數
} UARTSRemoStr_t;

/**
 * @brief ASA UART Slave0 結構初始化巨集
 * @ingroup asauart0_macro
 */
#define UART1SREGPARA                                                          \
    {                                                                          \
        .UARTDataReg_p = &UDR1, .UARTDataBytes = 1, .TXRXSwiReg_p = &PORTF,    \
        .TXRXSwiMask = 0x01, .TXRXSwiShift = 4, .TXEnReg_p = &UCSR1B,          \
        .TXEnMask = 0x10, .TXEnShift = 3, .RXEnReg_p = &UCSR1B,                \
        .RXEnMask = 0x40, .RXEnShift = 4, .TXIntEnReg_p = &UCSR1B,             \
        .TXIntEnMask = 0x08, .TXIntEnShift = 3, .RXIntEnReg_p = &UCSR1B,       \
        .RXIntEnMask = 0x80, .RXIntEnShift = 7, .TXIntClrReg_p = &UCSR1A,      \
        .TXIntClrMask = 0x40, .TXIntClrShift = 6, .RXIntClrReg_p = &UCSR1A,    \
        .RXIntClrMask = 0x80, .RXIntClrShift = 7, .TXIntSetReg_p = &UCSR1A,    \
        .TXIntSetMask = 0x40, .TXIntSetShift = 6, .RXIntSetReg_p = &UCSR1A,    \
        .RXIntSetMask = 0x80, .RXIntSetShift = 7, .TXIntFReg_p = &UCSR1A,      \
        .TXIntFMask = 0X20, .TXIntFShift = 5, .RXIntFReg_p = &UCSR1A,          \
        .RXIntFMask = 0x80, .RXIntFShift = 7, .UID = 0x80,                     \
        .Header = ASAUART_CMD_HEADER, .TimMax = 254                            \
    }
/**
 * @brief
 * 供使用者將函式登錄硬體中斷並取得編號，並且將函式登錄進排程以及將結構體連結至實體空間
 * @ingroup asauart0_macro
 */
#define UARTSRemoSTR_LAY(UARTREMOSTR, TXINTSTR, RXINTSTR, TOINTSTR,            \
                         PIPELINESTR, REMOBF)                                  \
    {                                                                          \
        UARTREMOSTR.TxTaskID =                                                 \
            HWInt_reg(&TXINTSTR, &UARTSRemoTX_step, &UARTREMOSTR);             \
        UARTREMOSTR.RxTaskID =                                                 \
            HWInt_reg(&RXINTSTR, &UARTSRemoRX_step, &UARTREMOSTR);             \
        UARTREMOSTR.TOTaskID =                                                 \
            HWInt_reg(&TOINTSTR, &UARTSRemoTO_step, &UARTREMOSTR);             \
        UARTREMOSTR.DDTaskID =                                                 \
            Pipeline_reg(&PIPELINESTR, &UARTSRemoDD_step, &UARTREMOSTR, 0);    \
        UARTREMOSTR.TOIntStr_p = &TOINTSTR;                                    \
        UARTREMOSTR.RemoBF_p   = &REMOBF;                                      \
        UARTREMOSTR.DataReg_p  = UARTREMOSTR.UARTDataReg_p;                    \
    }

/**
 * @brief ASA UART REMO 多位元組接收函式。
 *
 * @ingroup asauart_func
 * @param void_p    Uart Remo結構指標
 * @return          無
 */
uint8_t UARTSRemoRX_step(void* void_p);
/**
 * @brief ASA UART REMO 資料更新函式。
 *
 * @ingroup asauart_func
 * @param void_p    Uart Remo結構指標
 * @return          無
 */
uint8_t UARTSRemoDD_step(void* void_p);
/**
 * @brief ASA UART REMO 多位元組傳送函式。
 *
 * @ingroup asauart_func
 * @param void_p    Uart Remo結構指標
 * @return          無
 */
uint8_t UARTSRemoTX_step(void* void_p);
/**
 * @brief ASA UART REMO 逾時計數函式。
 *
 * @ingroup asauart_func
 * @param void_p    Uart Remo結構指標
 * @return          無
 */
uint8_t UARTSRemoTO_step(void* void_p);
/* Public Section End */
#endif  // C4MLIB_ASAUART_ASAUART_SLAVE_H

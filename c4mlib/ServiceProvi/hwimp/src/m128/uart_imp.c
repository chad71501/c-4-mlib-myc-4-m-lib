/**
 * @file uart_imp.c
 * @author 
 * @brief 
 * @date 2019.09.04
 * 
 */

#include "c4mlib/ServiceProvi/hwimp/src/m128/uart_imp.h"
HWIntStr_t *UartTxIntStrList_p[UART_HW_NUM];
HWIntStr_t *UartRxIntStrList_p[UART_HW_NUM];

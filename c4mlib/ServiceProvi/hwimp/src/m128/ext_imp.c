/**
 * @file ext_imp.c
 * @author LiYu87
 * @date 2019.11.14
 * @brief
 *
 */

#include "c4mlib/ServiceProvi/hwimp/src/m128/ext_imp.h"

HWIntStr_t* ExtIntStrList_p[EXT_HW_NUM];

#define IS_EXT_MODE(MODE)                                       \
    (((MODE) == EXT_MODE_LOW_LEVEL) | ((MODE) == EXT_MODE_FALLING) | \
    ((MODE) == EXT_MODE_RAISING))

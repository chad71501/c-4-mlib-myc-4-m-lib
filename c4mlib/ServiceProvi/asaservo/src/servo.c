/**
 * @file servo.c
 * @author 葉子哇 (w8indow61231111@gmail.com)
 * @date 2020.11.19
 * @brief
 *
 */
#ifdef __AVR_ATmega128__
#    include "m128/servo.c"
#endif

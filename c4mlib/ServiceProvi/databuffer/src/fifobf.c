/**
 * @file fifobf.c
 * @author YP-Shen , jerryhappyball
 * @date 2020.04.22
 * @brief
 * 實現先進先出緩衝區讀寫之功能，可提供使用者，前後級同時讀寫應用的資料緩衝。
 */

#include "c4mlib/ServiceProvi/databuffer/src/fifobf.h"

#include "c4mlib/C4MBios/device/src/device.h"

int8_t FifoBF_put(FifoBFStr_t* FBF_p, void* Data_p, uint8_t Bytes) {
    if (Bytes > FBF_p->Depth - FBF_p->Total)
        return (FBF_p->Depth - FBF_p->Total - Bytes);
    uint8_t sreg_temp = SREG;
    cli();
    for (uint8_t i = 0; i < Bytes; i++) {
        ((uint8_t*)FBF_p->List_p)[FBF_p->PIndex] = ((uint8_t*)Data_p)[i];
        FBF_p->Total++;
        FBF_p->PIndex++;
        if (FBF_p->PIndex == FBF_p->Depth) {
            FBF_p->PIndex = 0;
        }
    }
    SREG = sreg_temp;
    return (FBF_p->Depth - FBF_p->Total);
}

int8_t FifoBF_get(FifoBFStr_t* FBF_p, void* Data_p, uint8_t Bytes) {
    if (Bytes > FBF_p->Total)
        return (FBF_p->Total - Bytes);
    uint8_t sreg_temp = SREG;
    cli();
    for (uint8_t i = 0; i < Bytes; i++) {
        ((uint8_t*)Data_p)[i] = ((uint8_t*)FBF_p->List_p)[FBF_p->GIndex];
        FBF_p->Total--;
        FBF_p->GIndex++;
        if (FBF_p->GIndex == FBF_p->Depth) {
            FBF_p->GIndex = 0;
        }
    }
    SREG = sreg_temp;
    return (FBF_p->Total);
}

uint8_t FifoBF_clr(FifoBFStr_t* FBF_p) {
    FBF_p->Total  = 0;
    FBF_p->PIndex = 0;
    FBF_p->GIndex = 0;
    return (FBF_p->Depth);
}

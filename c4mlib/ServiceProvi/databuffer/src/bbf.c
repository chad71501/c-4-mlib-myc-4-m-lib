/**
 * @file bbf.c
 * @author jerryhappyball
 * @date 2020.04.22
 * @brief
 * 實現批次緩衝暫存器功能，包括資料寫入、資料讀取、強制設定暫存器為滿、強制清空暫存器。
 */

#include "c4mlib/ServiceProvi/databuffer/src/bbf.h"

#include "c4mlib/C4MBios/device/src/device.h"

#define BatchBufferEmpty 0
#define BatchBufferFull  1

int8_t BatchBF_put(BatchBFStr_t* BBF_p, void* Data_p, uint8_t Bytes) {
    if (BBF_p->State == BatchBufferFull ||
        (Bytes > BBF_p->Depth - BBF_p->Index))
        return (BBF_p->Depth - BBF_p->Index - Bytes);

    uint8_t sreg_temp = SREG;
    cli();
    for (uint8_t i = 0; i < Bytes; i++) {
        ((uint8_t*)BBF_p->List_p)[BBF_p->Index] = ((uint8_t*)Data_p)[i];
        BBF_p->Index++;
        BBF_p->Total++;
    }
    if (BBF_p->Index == BBF_p->Depth) {
        BBF_p->State = BatchBufferFull;
        BBF_p->Index = 0;
        SREG         = sreg_temp;
        return 0;
    }
    else {
        SREG = sreg_temp;
        return (BBF_p->Depth - BBF_p->Index);
    }
}

int8_t BatchBF_get(BatchBFStr_t* BBF_p, void* Data_p, uint8_t Bytes) {
    if (BBF_p->State == BatchBufferEmpty ||
        (Bytes > BBF_p->Total - BBF_p->Index))
        return (BBF_p->Total - BBF_p->Index - Bytes);
    uint8_t sreg_temp = SREG;
    cli();
    for (uint8_t i = 0; i < Bytes; i++) {
        ((uint8_t*)Data_p)[i] = ((uint8_t*)BBF_p->List_p)[BBF_p->Index];
        BBF_p->Index++;
    }
    if (BBF_p->Index == BBF_p->Total) {
        BBF_p->State = BatchBufferEmpty;
        BBF_p->Index = 0;
        BBF_p->Total = 0;
        SREG         = sreg_temp;
        return 0;
    }
    else {
        SREG = sreg_temp;
        return (BBF_p->Total - BBF_p->Index);
    }
}

uint8_t BatchBF_full(BatchBFStr_t* BBF_p) {
    BBF_p->Index = 0;
    BBF_p->State = BatchBufferFull;
    return (BBF_p->Total);
}

uint8_t BatchBF_clr(BatchBFStr_t* BBF_p) {
    BBF_p->Index = 0;
    BBF_p->Total = 0;
    BBF_p->State = BatchBufferEmpty;
    return (BBF_p->Depth);
}

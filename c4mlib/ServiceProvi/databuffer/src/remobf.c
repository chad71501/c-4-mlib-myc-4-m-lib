/**
 * @file remobf.c
 * @author Yi-Mou , jerryhappyball
 * @date 2021.06.17
 * @brief 提供remobf函式實作
 */

#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"

#include "c4mlib/C4MBios/debug/src/debug.h"

#include <avr/interrupt.h>
#include <avr/io.h>
uint8_t RemoBF_reg(RemoBFStr_t* Str_p, void* Data_p, uint8_t Bytes) {
    uint8_t VIndex = Str_p->VariTotal;

    Str_p->VList_p[VIndex].Vari_p = Data_p;
    Str_p->VList_p[VIndex].Bytes  = Bytes;
    Str_p->DataTotal += Bytes;
    if (Str_p->DataTotal >= Str_p->DataDepth) {
        DEBUG_INFO("Data overflow!!\n");
    }
    Str_p->VariTotal++;
    if (Str_p->VariTotal >= Str_p->VariDepth) {
        DEBUG_INFO("Variable overflow!!\n");
    }
    return VIndex;
}

uint8_t RemoBF_put(RemoBFStr_t* Str_p, uint8_t RegId) {
    if (Str_p->State != 1) {
        return 1;
    }
    uint8_t sreg_temp = SREG;
    cli();
    RemoVari_t* VList_p;
    Str_p->DataIndex = 0;
    if (RegId == 255) {
        for (uint8_t VIndex = 0; VIndex < Str_p->VariTotal; VIndex++) {
            VList_p = &Str_p->VList_p[VIndex];
            for (uint8_t i = 0; i < VList_p->Bytes; i++) {
                ((uint8_t*)VList_p->Vari_p)[i] =
                    Str_p->DList_p[Str_p->DataIndex];
                Str_p->DataIndex++;
            }
        }
        if (Str_p->DataIndex == Str_p->DataTotal) {
            Str_p->DataIndex = 0;
            Str_p->State     = 0;
        }
    }
    else {
        VList_p = &Str_p->VList_p[RegId];
        for (uint8_t i = 0; i < VList_p->Bytes; i++) {
            ((uint8_t*)VList_p->Vari_p)[i] = Str_p->DList_p[Str_p->DataIndex];
            Str_p->DataIndex++;
        }
        if (Str_p->DataIndex == VList_p->Bytes) {
            Str_p->DataIndex = 0;
            Str_p->State     = 0;
        }
    }
    SREG = sreg_temp;
    return 0;
}

int8_t RemoBF_temp(RemoBFStr_t* Str_p, uint8_t RegId, uint8_t Data) {
    RemoVari_t* VList_p;
    if (RegId == 255) {
        VList_p                          = &Str_p->VList_p[Str_p->VariIndex];
        Str_p->DList_p[Str_p->DataIndex] = Data;
        Str_p->DataIndex++;
        Str_p->ByteIndex++;
        if (Str_p->ByteIndex == VList_p->Bytes) {
            Str_p->VariIndex++;
            Str_p->ByteIndex = 0;
            if (Str_p->VariIndex == Str_p->VariTotal) {
                Str_p->VariIndex = 0;
                if (Str_p->DataIndex == Str_p->DataTotal) {
                    Str_p->State = 1;
                }
            }
        }
        return Str_p->DataTotal - Str_p->DataIndex;
    }
    else {
        VList_p                          = &Str_p->VList_p[RegId];
        Str_p->DList_p[Str_p->DataIndex] = Data;
        Str_p->DataIndex++;
        if (Str_p->DataIndex == VList_p->Bytes) {
            Str_p->State     = 1;
            Str_p->DataIndex = 0;
            return 0;
        }

        return VList_p->Bytes - Str_p->DataIndex;
    }
}

int8_t RemoBF_get(RemoBFStr_t* Str_p, uint8_t RegId, void* Data_p) {
    RemoVari_t* VList_p;
    if (RegId == 255) {
        VList_p           = &Str_p->VList_p[Str_p->VariIndex];
        *(uint8_t*)Data_p = ((uint8_t*)VList_p->Vari_p)[Str_p->ByteIndex];
        Str_p->DataIndex++;
        Str_p->ByteIndex++;
        if (Str_p->ByteIndex == VList_p->Bytes) {
            Str_p->VariIndex++;
            Str_p->ByteIndex = 0;
            if (Str_p->VariIndex == Str_p->VariTotal) {
                Str_p->VariIndex = 0;
                Str_p->DataIndex = 0;
                return 0;
            }
        }
        return Str_p->DataTotal - Str_p->DataIndex;
    }
    else {
        VList_p           = &Str_p->VList_p[RegId];
        *(uint8_t*)Data_p = ((uint8_t*)VList_p->Vari_p)[Str_p->ByteIndex];
        Str_p->DataIndex++;
        Str_p->ByteIndex++;
        if (Str_p->ByteIndex == VList_p->Bytes) {
            Str_p->ByteIndex = 0;
            Str_p->DataIndex = 0;
            return 0;
        }
        return VList_p->Bytes - Str_p->DataIndex;
    }
}

uint8_t RemoBF_clr(RemoBFStr_t* Str_p) {
    Str_p->State     = 0;
    Str_p->ByteIndex = 0;
    Str_p->VariIndex = 0;
    Str_p->DataIndex = 0;
    return 0;
}

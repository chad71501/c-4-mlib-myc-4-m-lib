/**
 * @file test_bbf_8_bit.c
 * @author jerryhappyball
 * @date 2020-03-17
 * @brief BatchBuffer 8 bit data test
 */

#include "c4mlib/ServiceProvi/databuffer/src/bbf.h"
#include "c4mlib/C4MBios/device/src/device.h"

int main() {
    C4M_DEVICE_set();

    BATCHBUFF_LAY(ABBF_str, 16);

    /// Write1:
    uint8_t FrontData[2] = {1, 3}, FrontData2[3] = {5, 7, 10};
    BatchBF_put(&ABBF_str, FrontData, 2);  //寫入第一次資料

    printf("BBF_clr return = %d\n", BatchBF_clr(&ABBF_str));  //清除已寫入資料

    /// Write2:
    printf("BBF_put return = %d\n",
           BatchBF_put(&ABBF_str, FrontData2, 3));  //寫入第二次資料
    printf("Write:{");
    for (int i = 0; i < ABBF_str.Index; i++) {
        if (!i)
            printf("%d", ((uint8_t*)ABBF_str.List_p)[i]);
        else
            printf(",%d", ((uint8_t*)ABBF_str.List_p)[i]);
    }
    printf("}\n");

    printf("BBF_full return = %d\n", BatchBF_full(&ABBF_str));  //強制為滿

    /// Read1:
    uint8_t RearData[2] = {0}, RearData2[2] = {0};
    printf("BBF_get return = %d\n",
           BatchBF_get(&ABBF_str, RearData, 2));  //讀出第一次資料
    printf("Read:{");
    for (int i = 0; i < 2; i++) {
        if (!i)
            printf("%d", RearData[i]);
        else
            printf(",%d", RearData[i]);
    }
    printf("}\n");

    /// Read2:
    BatchBF_get(&ABBF_str, RearData2, 1);  //讀出第二次資料
    printf("Read:{");
    for (int i = 0; i < 2; i++) {
        if (!i)
            printf("%d", RearData2[i]);
        else
            printf(",%d", RearData2[i]);
    }
    printf("}\n");
}

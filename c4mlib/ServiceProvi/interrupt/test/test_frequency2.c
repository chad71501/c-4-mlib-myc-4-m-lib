/**
 * @file test_frequency2.c
 * @author smallplayplay
 * @date 2021.5.21
 * @brief
 *  將降頻處理器登入HWint底下，驅動伺服機
 * 步驟:
 *      TIMER2設定
 *      使用TIMHWINT_LAY進行TIMER的登記中斷的callback設定
 *      使用FREQREDU_LAY進行降頻的初始化設定
 *      使用FreqRedu_reg設定工作內容，並使用FreqRedu_en致能
 *      使用HWInt_en將中斷分享器致能、TIMER2中斷致能
 * 		使用scanf分別致能id0,id1或id2,id3這兩組不同的工作
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "c4mlib/C4MBios/macro/src/std_def.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/hwimp/src/layout_macro.h"
#include "c4mlib/ServiceProvi/interrupt/src/frequency_set.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#include "c4mlib/ServiceProvi/interrupt/test/test_interrupt_tim.cfg"

#define PERIPHERAL_INTERRUPT_ENABLE 255
#define PORT_B_7                    128
#define CYCLE                       200

uint8_t USER_FUNCTION(void* pvData_p);

uint8_t TIM2_init(void) {
    HardWareSet_t TIM2HWSet_str      = TIM2HWSETSTRINI;
    HWFlagPara_t TIM2FgGpData_str[5] = TIM2FLAGPARALISTINI;
    HWRegPara_t TIM2RegData_str      = TIM2REGPARALISTINI;
    TIM2HWSetDataStr_t TIM2HWSetData = TIM2SETDATALISTINI;
    HARDWARESET_LAY(TIM2HWSet_str, TIM2FgGpData_str[0], TIM2RegData_str,
                    TIM2HWSetData)
    return HardwareSet_step(&TIM2HWSet_str);
}

int time = 1;
int main(void) {
    C4M_DEVICE_set();

    TIM2_init();

    TIMOpStr_t HWOPSTR = TIM2OPSTRINI;
    TIMHWINT_LAY(freqredu4isr, 2, 1, HWOPSTR);
    uint8_t period[2] = {252, 252};
    FREQREDU_LAY(freqredustr, 4, CYCLE, &OCR2, 0, period);
    uint16_t id0 = 0, id1 = 0, id2 = 0, id3 = 0, atm1 = 0, atm2 = 10;
    id0 = FreqRedu_reg(&freqredustr, &USER_FUNCTION, &atm1, 1, 1);
    id1 = FreqRedu_reg(&freqredustr, &USER_FUNCTION, &atm2, 1, 8);
    id2 = FreqRedu_reg(&freqredustr, &USER_FUNCTION, &atm1, 1, 1);
    id3 = FreqRedu_reg(&freqredustr, &USER_FUNCTION, &atm2, 1, 12);

    FreqRedu_en(&freqredustr, id0, ENABLE);
    FreqRedu_en(&freqredustr, id1, ENABLE);

    uint8_t TaskID = HWInt_reg(&freqredu4isr, &FreqRedu_step, &freqredustr);
    HWInt_en(&freqredu4isr, PERIPHERAL_INTERRUPT_ENABLE,
             ENABLE);  //中斷分享器禁致能
    HWInt_en(&freqredu4isr, TaskID, ENABLE);
    sei();
    while (1) {
        printf("time = ");
        scanf("%d", &time);
        _delay_ms(50);
        if (time == 1) {
            FreqRedu_en(&freqredustr, id0, ENABLE);
            FreqRedu_en(&freqredustr, id1, ENABLE);
            FreqRedu_en(&freqredustr, id2, DISABLE);
            FreqRedu_en(&freqredustr, id3, DISABLE);
        }
        if (time == -1) {
            FreqRedu_en(&freqredustr, id0, DISABLE);
            FreqRedu_en(&freqredustr, id1, DISABLE);
            FreqRedu_en(&freqredustr, id2, ENABLE);
            FreqRedu_en(&freqredustr, id3, ENABLE);
        }
    }
}
uint8_t USER_FUNCTION(void* pvData_p) {
    if (pvData_p != NULL) {
        uint8_t* p_param = (uint8_t*)pvData_p;
        switch (*p_param) {
            case 0:
                REGFPT(&PORTB, PORT_B_7, 0, PORT_B_7);
                break;
            case 10:
                REGFPT(&PORTB, PORT_B_7, 0, 0);
                break;
        }
    }
    return 0;
}

/**
 * @file test_sspi_remop.c
 * @author Yi-Mou (sourslemon@g.ncu.edu.tw)
 * @brief 
 * @date 2021.01.25
 * 
 * 測試sspi_remo功能 需與test_mspi_remop共同測試
 * 兩邊將交換一次資料
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/hwimp/src/layout_macro.h"
#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/config/spi.cfg"
#include "c4mlib/ServiceProvi/remoptxrx/src/asaspi_remo_slave.h"
#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/C4MBios/hardwareset/src/spi_set.h"

#define BCDDEPTH  2
#define BCVSDEPTH 1
#define DLDDEPTH  2
#define DLVSDEPTH 1
#define ULDDEPTH  2
#define ULVSDEPTH 1
#define PERIPHERAL_INTERRUPT_ENABLE 255

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPISETDATALISTINI;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void){
    C4M_DEVICE_set();
    printf("start!\n");
    SPI_init();
    SPCR &= ~(1<<MSTR); //choose slave mode
    REGFPT(&DDRB,0x0f,0,0x08);
    SPDR = 0;
    SSPIREMOP_LAY(remosspi_str,&SPDR,BCBFSTR,BCDDEPTH ,BCVSDEPTH ,DLBFSTR,DLDDEPTH,DLVSDEPTH,ULBFSTR,ULDDEPTH,ULVSDEPTH);
    
    uint16_t bcdata=0;
    uint16_t dldata = 0;
    uint16_t updata = 0xDADA;
    RemoBF_reg(remosspi_str.BCRemoBF_p, &bcdata, 2);
    RemoBF_reg(remosspi_str.DLRemoBF_p, &dldata, 2);
    RemoBF_reg(remosspi_str.ULRemoBF_p, &updata, 2);

    SPIOpStr_t HWOPSTR = SPIOPSTRINI;
    SPIHWINT_LAY(SpiInt_Str, 0, 2, HWOPSTR);
    remosspi_str.SpiTaskId = HWInt_reg(&SpiInt_Str,&SSpiRemoPTXRX_step, &remosspi_str);
    remosspi_str.Spi_ssp = &SpiInt_Str;
    HWInt_en(&SpiInt_Str, PERIPHERAL_INTERRUPT_ENABLE,1);
    HWInt_en(&SpiInt_Str,remosspi_str.SpiTaskId,1);

    sei();
    while(1){
        printf("bcdata = %X  dldata = %X state = %d \n",bcdata,dldata,remosspi_str.State);
        _delay_ms(200);
	}
    return 0;
}




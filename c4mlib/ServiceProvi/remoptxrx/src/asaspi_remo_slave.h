/**
 * @file asaspi_remo_master.h
 * @author Yi-Mou (sourslemon@g.ncu.edu.tw)
 * @brief
 * @date 2020.07.05
 *
 *
 */
#ifndef C4MLIB_ASASPI_ASASPI_REMO_SLAVE_H
#define C4MLIB_ASASPI_ASASPI_REMO_SLAVE_H

#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"

/**
 * @defgroup asaspi_func
 * @defgroup asaspi_macro
 * @defgroup asaspi_struct
 */

/* Public Section Start */
/**
 * @brief SSpiRemoStr_t結構原型
 *
 * @ingroup asaspi_struct
 */
typedef struct {
    uint8_t RegAddr;          ///< 本次存取遠端變數編號
    uint8_t RWMode;           ///< 本次通訊的存取方向
    uint8_t State;            ///< 通訊封包狀態機
    uint8_t OutData;          ///< 下次通訊要回傳的值
    uint8_t ErrorCount;       ///< 封包檢查的錯誤數量
    uint8_t Residual;         ///< 資料剩餘筆數
    void* SpiReg_p;           ///< spi_data_reg指標
    HWIntStr_t* ssp;          ///< 硬體中斷登錄結構指標
    uint8_t TaskId;           ///< 硬體中斷登錄編號
    RemoBFStr_t* RXRemoBF_p;  ///< 接收用remoBF
    RemoBFStr_t* TXRemoBF_p;  ///< 傳送用remoBF
} SSpiRemoStr_t;

#define SSpiRemoSTR_LAY(REMOSTR, SPIREGA, TXDDEPTH, TXVSDEPTH, RXDDEPTH,       \
                        RXVSDEPTH)                                             \
    SSpiRemoStr_t REMOSTR = {0};                                               \
    {                                                                          \
        REMOBUFF_LAY(TXBFSTR, TXDDEPTH, TXVSDEPTH);                            \
        REMOBUFF_LAY(RXBFSTR, RXDDEPTH, RXVSDEPTH);                            \
        REMOSTR.SpiReg_p   = (char*)SPIREGA;                                   \
        REMOSTR.RXRemoBF_p = &RXBFSTR;                                         \
        REMOSTR.TXRemoBF_p = &TXBFSTR;                                         \
        uint8_t TaskId =                                                       \
            HWInt_reg(SpiIntStrList_p, SSpiRemoTXRX_step, &REMOSTR);           \
        REMOSTR.TaskId  = TaskId;                                              \
        REMOSTR.Spi_ssp = SpiIntStrList_p;                                     \
    }

uint8_t SSpiRemoRX_step(void* Str_p);

/**
 * @brief
 * @ingroup asaspi_struct
 */
typedef struct {
    uint8_t OldData;   ///< 前一次傳輸的資料
    uint8_t FeedBack;  ///< master傳來的值
    uint8_t OKNG;
    uint8_t ChkSum;           ///< 紀錄傳送資料的chksum
    uint8_t Residual;         ///< 紀錄資料剩餘筆數
    uint8_t State;            ///< 封包通訊狀態
    void* SpiReg_p;           ///< spi_data_reg指標
    HWIntStr_t* Spi_ssp;      ///< 硬體中斷登錄結構指標
    uint8_t SpiTaskId;        ///< 硬體中斷登錄編號
    RemoBFStr_t* BCRemoBF_p;  ///< 廣播用remoBF
    RemoBFStr_t* DLRemoBF_p;  ///< 下行鏈結用remoBF
    RemoBFStr_t* ULRemoBF_p;  ///< 上行鏈結用remoBF
} SSpiRemoPStr_t;
/**
 * @brief SSpiRemoP接收函式
 *
 * @param void_p SSpiRemoPStr_t結構指標
 */
uint8_t SSpiRemoPTXRX_step(void* void_p);

/**
 * @brief SSPIREMOP佈線巨集
 *
 * @param REMOSTR 建構的結構體名稱
 * @param SPIREGA SPI_Data_Reg指標
 * @param BCBFSTR 廣播Remo結構體名稱
 * @param BCDDEPTH 廣播Remo結構體最大資料長度
 * @param BCVSDEPTH 廣播Remo結構體最大變數數量
 * @param DLBFSTR 下行鏈結Remo結構體名稱
 * @param DLDDEPTH 下行鏈結Remo結構體最大資料長度
 * @param DLVSDEPTH 下行鏈結Remo結構體最大變數數量
 * @param ULBFSTR 上行鏈結Remo結構體名稱
 * @param ULDDEPTH 上行鏈結Remo結構體最大資料長度
 * @param ULVSDEPTH 上行鏈結Remo結構體最大變數數量
 */
#define SSPIREMOP_LAY(REMOSTR, SPIREGA, BCBFSTR, BCDDEPTH, BCVSDEPTH, DLBFSTR, \
                      DLDDEPTH, DLVSDEPTH, ULBFSTR, ULDDEPTH, ULVSDEPTH)       \
    REMOBUFF_LAY(BCBFSTR, BCDDEPTH, BCVSDEPTH);                                \
    REMOBUFF_LAY(DLBFSTR, DLDDEPTH, DLVSDEPTH);                                \
    REMOBUFF_LAY(ULBFSTR, ULDDEPTH, ULVSDEPTH);                                \
    SSpiRemoPStr_t REMOSTR = {0};                                              \
    REMOSTR.SpiReg_p       = (char*)SPIREGA;                                   \
    REMOSTR.BCRemoBF_p     = &BCBFSTR;                                         \
    REMOSTR.DLRemoBF_p     = &DLBFSTR;                                         \
    REMOSTR.ULRemoBF_p     = &ULBFSTR;

/* Public Section End */

#endif  // C4MLIB_ASAUART_ASAUART_REMO_SLAVE_H

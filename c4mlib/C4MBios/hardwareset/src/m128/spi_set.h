/**
 * @file spi_set.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.13
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARESET_M128_SPI_SET_H
#define C4MLIB_HARDWARESET_M128_SPI_SET_H

#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/macro/src/std_def.h"

#include <stdint.h>

/* Public Section Start */
/**
 * @brief
 * @param SPI2SPEED      //Double SPI Speed
 * @param SPR0_1         //Clock Prescale
 * @param CPHASE         //SynchRdWt
 * @param CPOLAR         //SynchRiseDown
 * @param MSSELECT       //MSSelect
 * @param DORDER         //ByteOrder
 * @param DDx0_3         //MOSI, SCK, /SS set output. MISO set input.
 * @param SPIEN          //SPI Enable
 * @param SPIINTEN       //SPI Interrupt Enable
 * @param FlagTotalBytes //Total Bytes of Flag Group Datum
 * @param RegTotalBytes  //Total Bytes of Registers
 */
typedef struct {
    uint8_t DDx0_3;
    uint8_t ASA_ADDR;
    uint8_t SPI_CS8_B;
    uint8_t SPI2SPEED;
    uint8_t SPR0_1;
    uint8_t CPHASE;
    uint8_t CPOLAR;
    uint8_t MSSELECT;
    uint8_t DORDER;
    uint8_t SPIEN;
    uint8_t SPIINTEN;
    uint8_t FlagTotalBytes;
    uint8_t RegTotalBytes;
} SPIHWSetDataStr_t;

#define SPIFLAGPARALISTINI                                                     \
    {                                                                          \
        {.Reg_p = &DDRB, .Mask = 0x0F, .Shift = 0},                            \
        {.Reg_p = &DDRF, .Mask = 0xE0, .Shift = 5},                            \
        {.Reg_p = &DDRB, .Mask = 0xF0, .Shift = 4},                            \
        {.Reg_p = &SPSR, .Mask = 0x01, .Shift = 0},                            \
        {.Reg_p = &SPCR, .Mask = 0x03, .Shift = 0},                            \
        {.Reg_p = &SPCR, .Mask = 0x04, .Shift = 2},                            \
        {.Reg_p = &SPCR, .Mask = 0x08, .Shift = 3},                            \
        {.Reg_p = &SPCR, .Mask = 0x10, .Shift = 4},                            \
        {.Reg_p = &SPCR, .Mask = 0x20, .Shift = 5},                            \
        {.Reg_p = &SPCR, .Mask = 0x40, .Shift = 6},                            \
        {.Reg_p = &SPCR, .Mask = 0x80, .Shift = 7}                             \
    }

#define SPIREGPARALISTINI                                                      \
    { 0 }

#define SPIHWSETSTRINI                                                         \
    {.FlagNum = 11, .RegNum = 0}

/**
 * @brief
 * @param SPIDataReg_p SPI Data I/O Reg address
 * @param SPIDataBytes SPI Data I/O Reg Bytes
 * @param SPIEnReg_p SPI Enable flag Reg address
 * @param SPIEnMask SPI Enable flag Mask, shift
 * @param SPIEnShift
 * @param IntEnReg_p Interrupt Enable flag Reg address
 * @param IntEnMask Interrupt Enable flag Mask, shift
 * @param IntEnShift
 * @param SPIActReg_pSPI In Action flag Reg address
 * @param SPIActMask SPI In Action flag Mask, shift
 * @param SPIActShift
 * @param IntSetReg_p Interrupt flag Set Reg address
 * @param IntSetMask Interrupt flag Set Mask, shift
 * @param IntSetShift
 *
 */
typedef struct {
    volatile uint8_t* SPIDataReg_p;
    uint8_t SPIDataBytes;
    volatile uint8_t* SPIEnReg_p;
    uint8_t SPIEnMask;
    uint8_t SPIEnShift;
    volatile uint8_t* IntEnReg_p;
    uint8_t IntEnMask;
    uint8_t IntEnShift;
    volatile uint8_t* SPIActReg_p;
    uint8_t SPIActMask;
    uint8_t SPIActShift;
    volatile uint8_t* IntSetReg_p;
    uint8_t IntSetMask;
    uint8_t IntSetShift;
} SPIOpStr_t;

#define SPIOPSTRINI                                                            \
    {                                                                          \
        .SPIDataReg_p = &SPDR, .SPIDataBytes = 1, .SPIEnReg_p = &SPCR,         \
        .SPIEnMask = 0x40, .SPIEnShift = 6, .IntEnReg_p = &SPCR,               \
        .IntEnMask = 0x80, .IntEnShift = 7, .SPIActReg_p = &SPSR,              \
        .SPIActMask = 0x40, .SPIActShift = 6, .IntSetReg_p = &SPSR,            \
        .IntSetMask = 0x80, .IntSetShift = 7                                   \
    }

/* Public Section End */
#endif  // C4MLIB_HARDWARESET_M128_SPI_SET_H

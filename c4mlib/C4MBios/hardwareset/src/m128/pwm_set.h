/**
 * @file pwm_set.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.13
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARESET_M128_PWM_SET_H
#define C4MLIB_HARDWARESET_M128_PWM_SET_H

#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_def.h"

#include <stdint.h>
/* Public Section Start */

/**
 * @brief PWM0
 *
 * @param ASn             // ClockSource
 * @param WGMn0_1         // WaveSelection
 * @param CSn0_2          // PWME0FreqDivide
 * @param COMn0_1         // PWME0WareOut
 * @param DDx             // WaveOutPin
 * @param PWM0IEN         // PWM interrupt enable
 * @param FlagTotalBytes  // Total Bytes of Flag Group Datum
 * @param OCRn            // Adjust PWMER0 Cycle
 * @param RegTotalBytes   // Total Bytes of Registers
 *
 */
typedef struct {
    uint8_t ASn;
    uint8_t WGMn0_1;
    uint8_t CSn0_2;
    uint8_t COMn0_1;
    uint8_t DDx;
    uint8_t PWM0IEN;
    uint8_t FlagTotalBytes;
    uint8_t OCRn;
    uint8_t RegTotalBytes;
} PWM0HWSetDataStr_t;

/**
 * @brief PWM1 or PWM3
 *
 * @param WGMn0_1         // WaveSelection
 * @param WGMn2_3         // WaveSelection
 * @param CSn0_2          // PWMEnFreqDivide
 * @param COMnA0_1        // PWMEnWareOut
 * @param COMnB0_1        // PWMEnWareOut
 * @param COMnC0_1        // PWMEnWareOut
 * @param DDxA            // WaveOutPin
 * @param DDxB            // WaveOutPin
 * @param DDxC            // WaveOutPin
 * @param PWMnIEN         // PWMn Interrupt Enable
 * @param FlagTotalBytes  // Total Bytes of Flag Group Datum
 * @param ICRn            // PWMMaxWidth
 * @param OCRnA           // PWMPulseWidth
 * @param OCRnB           // PWMPulseWidth
 * @param OCRnC           // PWMPulseWidth
 * @param RegTotalBytes   // Total Bytes of Registers
 */
typedef struct {
    uint8_t WGMn0_1;
    uint8_t WGMn2_3;
    uint8_t CSn0_2;
    uint8_t COMnA0_1;
    uint8_t COMnB0_1;
    uint8_t COMnC0_1;
    uint8_t DDxA;
    uint8_t DDxB;
    uint8_t DDxC;
    uint8_t PWM1IEN;
    uint8_t FlagTotalBytes;
    uint16_t ICRn;
    uint16_t OCRnA;
    uint16_t OCRnB;
    uint16_t OCRnC;
    uint8_t RegTotalBytes;
} PWM1HWSetDataStr_t;

typedef struct {
    uint8_t WGMn0_1;
    uint8_t WGMn2_3;
    uint8_t CSn0_2;
    uint8_t COMnA0_1;
    uint8_t COMnB0_1;
    uint8_t COMnC0_1;
    uint8_t DDxA;
    uint8_t DDxB;
    uint8_t DDxC;
    uint8_t PWM3IEN;
    uint8_t FlagTotalBytes;
    uint16_t ICRn;
    uint16_t OCRnA;
    uint16_t OCRnB;
    uint16_t OCRnC;
    uint8_t RegTotalBytes;
} PWM3HWSetDataStr_t;

/**
 * @brief  PWM2
 *
 * @param WGMn0_1         // WaveSelection
 * @param CSn0_2          // PWME0FreqDivide
 * @param COMn0_1         // PWME0WareOut
 * @param DDx             // WaveOutPin
 * @param PWM2IEN         // PWM2 Interrupt Enable
 * @param FlagTotalBytes  // Total Bytes of Flag Group Datum
 * @param OCRn            // Adjust PWMER0 Cycle
 * @param RegTotalBytes   // Total Bytes of Registers
 */
typedef struct {
    uint8_t WGMn0_1;
    uint8_t CSn0_2;
    uint8_t COMn0_1;
    uint8_t DDx;
    uint8_t PWM2IEN;
    uint8_t FlagTotalBytes;
    uint8_t OCRn;
    uint8_t RegTotalBytes;
} PWM2HWSetDataStr_t;

#define PWM0FLAGPARALISTINI                                                    \
    {                                                                          \
        {.Reg_p = &ASSR, .Mask = 0x08, .Shift = 3},                            \
        {.Reg_p = &TCCR0, .Mask = 0x48, .Shift = 3},                           \
        {.Reg_p = &TCCR0, .Mask = 0x07, .Shift = 0},                           \
        {.Reg_p = &TCCR0, .Mask = 0x30, .Shift = 4},                           \
        {.Reg_p = &DDRB, .Mask = 0x10, .Shift = 4},                            \
        {.Reg_p = &TIMSK, .Mask = 0x01, .Shift = 0}                            \
    }

#define PWM1FLAGPARALISTINI                                                    \
    {                                                                          \
        {.Reg_p = &TCCR1A, .Mask = 0x03, .Shift = 0},                          \
        {.Reg_p = &TCCR1B, .Mask = 0x18, .Shift = 3},                          \
        {.Reg_p = &TCCR1B, .Mask = 0x07, .Shift = 0},                          \
        {.Reg_p = &TCCR1A, .Mask = 0xc0, .Shift = 6},                          \
        {.Reg_p = &TCCR1A, .Mask = 0x30, .Shift = 4},                          \
        {.Reg_p = &TCCR1A, .Mask = 0x0c, .Shift = 2},                          \
        {.Reg_p = &DDRB, .Mask = 0xe0, .Shift = 5},                            \
        {.Reg_p = &DDRB, .Mask = 0x40, .Shift = 6},                            \
        {.Reg_p = &DDRB, .Mask = 0x80, .Shift = 7},                            \
        {.Reg_p = &TIMSK, .Mask = 0x01, .Shift = 1}                            \
    }

#define PWM2FLAGPARALISTINI                                                    \
    {                                                                          \
        {.Reg_p = &TCCR2, .Mask = 0x48, .Shift = 3},                           \
        {.Reg_p = &TCCR2, .Mask = 0x07, .Shift = 0},                           \
        {.Reg_p = &TCCR2, .Mask = 0x30, .Shift = 4},                           \
        {.Reg_p = &DDRB, .Mask = 0x80, .Shift = 7},                            \
        {.Reg_p = &TIMSK, .Mask = 0x40, .Shift = 6}                            \
    }

#define PWM3FLAGPARALISTINI                                                    \
    {                                                                          \
        {.Reg_p = &TCCR3A, .Mask = 0x03, .Shift = 0},                          \
        {.Reg_p = &TCCR3B, .Mask = 0x18, .Shift = 3},                          \
        {.Reg_p = &TCCR3B, .Mask = 0x07, .Shift = 0},                          \
        {.Reg_p = &TCCR3A, .Mask = 0xc0, .Shift = 6},                          \
        {.Reg_p = &TCCR3A, .Mask = 0x30, .Shift = 4},                          \
        {.Reg_p = &TCCR3A, .Mask = 0x0c, .Shift = 2},                          \
        {.Reg_p = &DDRB, .Mask = 0x08, .Shift = 3},                            \
        {.Reg_p = &DDRB, .Mask = 0x10, .Shift = 4},                            \
        {.Reg_p = &DDRB, .Mask = 0x20, .Shift = 5},                            \
        {.Reg_p = &ETIMSK, .Mask = 0x02, .Shift = 1}                           \
    }

#define PWM0REGPARALISTINI                                                     \
    { .Reg_p = &OCR0, .Bytes = 1 }

#define PWM1REGPARALISTINI                                                     \
    {                                                                          \
        {.Reg_p = &ICR1L, .Bytes = 2}, {.Reg_p = &OCR1AL, .Bytes = 2},         \
        {.Reg_p = &OCR1BL, .Bytes = 2}, {.Reg_p = &OCR1CL, .Bytes = 2}         \
    }

#define PWM2REGPARALISTINI                                                     \
    { .Reg_p = &OCR2, .Bytes = 1 }

#define PWM3REGPARALISTINI                                                     \
    {                                                                          \
        {.Reg_p = &ICR3L, .Bytes = 2}, {.Reg_p = &OCR3AL, .Bytes = 2},         \
        {.Reg_p = &OCR3BL, .Bytes = 2}, {.Reg_p = &OCR3CL, .Bytes = 2}         \
    }

#define PWM0HWSETSTRINI                                                        \
    {.FlagNum = 6, .RegNum = 1}

#define PWM1HWSETSTRINI                                                        \
    {.FlagNum = 10, .RegNum = 4}

#define PWM2HWSETSTRINI                                                        \
    {.FlagNum = 5, .RegNum = 1}

#define PWM3HWSETSTRINI                                                        \
    {.FlagNum = 10, .RegNum = 4}

/**
 * @brief
 * @param IntEnReg_p Interrupt Enable flag Reg address
 * @param IntEnMask Interrupt Enable flag Mask, shift
 * @param IntEnShift
 * @param IntSetReg_p Interrupt flag Set Reg address
 * @param IntSetMask Interrupt flag Set Mask, shift
 * @param IntSetShift
 * @param CountReg_p PWM Counter Register addr
 * @param CountBytes PWM Counter bytes
 * @param WidthReg_p PWM Width set Register addr
 * @param WidthBytes PWM Width set byte *
 */
typedef struct {
    volatile uint8_t* IntEnReg_p;
    uint8_t IntEnMask;
    uint8_t IntEnShift;
    volatile uint8_t* IntSetReg_p;
    uint8_t IntSetMask;
    uint8_t IntSetShift;
    volatile uint8_t* CountReg_p;
    uint8_t CountBytes;
    volatile uint8_t* WidthReg_p;
    uint8_t WidthBytes;
} PWMOpStr_t;

#define PWM0OPSTRINI                                                           \
    {                                                                          \
        .IntEnReg_p = &TIMSK, .IntEnMask = 0x01, .IntEnShift = 0,              \
        .IntSetReg_p = &TIFR, .IntSetMask = 0x01, .IntSetShift = 0,            \
        .CountReg_p = &TCNT0, .CountBytes = 1, .WidthReg_p = &OCR0,            \
        .WidthBytes = 1                                                        \
    }

#define PWM1OPSTRINI                                                           \
    {                                                                          \
        .IntEnReg_p = &TIMSK, .IntEnMask = 0x04, .IntEnShift = 2,              \
        .IntSetReg_p = &TIFR, .IntSetMask = 0x04, .IntSetShift = 2,            \
        .CountReg_p = &TCNT1L, .CountBytes = 2, .WidthReg_p = &OCR1AL,         \
        .WidthBytes = 2                                                        \
    }

#define PWM2OPSTRINI                                                           \
    {                                                                          \
        .IntEnReg_p = &TIMSK, .IntEnMask = 0x40, .IntEnShift = 6,              \
        .IntSetReg_p = &TIFR, .IntSetMask = 0x40, .IntSetShift = 6,            \
        .CountReg_p = &TCNT2, .CountBytes = 1, .WidthReg_p = &OCR2,            \
        .WidthBytes = 1                                                        \
    }

#define PWM3OPSTRINI                                                           \
    {                                                                          \
        .IntEnReg_p = &ETIMSK, .IntEnMask = 0x04, .IntEnShift = 2,             \
        .IntSetReg_p = &ETIFR, .IntSetMask = 0x10, .IntSetShift = 4,           \
        .CountReg_p = &TCNT3L, .CountBytes = 2, .WidthReg_p = &OCR3AL,         \
        .WidthBytes = 2                                                        \
    }

/* Public Section End */

#endif  // C4MLIB_HARDWAREINI_M128_PWM_SET_H

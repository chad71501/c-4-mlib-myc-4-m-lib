/**
 * @file test_tim0_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.15
 * @brief 
 * 
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/config/tim.cfg"
#include "c4mlib/C4MBios/hardwareset/src/m128/tim_set.h"

uint8_t TIM0_init(void){
    HardWareSet_t TIM0HWSet_str = TIM0HWSETSTRINI;
    HWFlagPara_t TIM0FgGpData_str[6] = TIM0FLAGPARALISTINI;
    HWRegPara_t TIM0RegData_str = TIM0REGPARALISTINI;
    TIM0HWSetDataStr_t TIM0HWSetData = TIM0SETDATALISTINI;
    HARDWARESET_LAY(TIM0HWSet_str, TIM0FgGpData_str[0], TIM0RegData_str, TIM0HWSetData)
    return HardwareSet_step(&TIM0HWSet_str);
}

int main() {
    C4M_DEVICE_set();

    uint8_t a = TIM0_init();
    printf("hardware_set report %d \n", a);
    printf("ASn = %d\n", ((ASSR & 0x08) >> 3));
    printf("WGMn0_1 = %d\n", ((TCCR0 & 0x48) >> 3));
    printf("CSn0_2 = %d\n", ((TCCR0 & 0x07) >> 0));
    printf("COMn0_1 = %d\n", ((TCCR0 & 0x30) >> 4));
    printf("DDx = %d\n", ((DDRB & 0x10) >> 4));
    printf("OCR0 = %d\n", OCR0);
    printf("TIMSK = %d\n", (TIMSK & 0x02) >> 1);
}

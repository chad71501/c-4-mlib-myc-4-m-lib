/**
 * @file test_REG_macro2.c
 * @author cy023
 * @date 2021.01.28
 * @brief 測試 HF_REGPUT, LF_REGPUT, LF_REGGET, HF_REGGET macro
 */

#include "c4mlib/C4MBios/device/src/device.h"

#include <avr/io.h>

int main() {
    C4M_DEVICE_set();
    unsigned char send_data[2], recv_data[2];
    uint16_t data = 306, rec;
    send_data[0] = LOBYTE16(data);
    send_data[1] = HIBYTE16(data);
    
    printf("===== HF_REGPUT + LF_REGGET TEST =====\n");
    printf("send_data : %d %d\n", send_data[0], send_data[1]);
    HF_REGPUT(&OCR1AL, 2, send_data);
    LF_REGGET(&OCR1AL, 2, recv_data);
    printf("recv_data : %d %d\n", recv_data[0], recv_data[1]);
    
    rec = (recv_data[0] | (uint16_t)(recv_data[1] << 8));
    printf("%d, %d\n\n", OCR1A, rec);

    printf("===== LF_REGPUT + HF_REGGET TEST =====\n");
    printf("send_data : %d %d\n", send_data[0], send_data[1]);
    LF_REGPUT(&OCR1AL, 2, send_data);
    HF_REGGET(&OCR1AL, 2, recv_data);
    printf("recv_data : %d %d\n", recv_data[0], recv_data[1]);
    
    rec = (recv_data[0] | (uint16_t)(recv_data[1] << 8));
    printf("%d, %d", OCR1A, rec);

    return 0;
}

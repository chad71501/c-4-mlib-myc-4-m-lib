/**
 * @file std_res.h
 * @author LiYu87
 * @date 2019.03.25
 * @author ya058764
 * @date 2021.06.05
 * @brief 提供標準型態給函式庫使用。
 */

#ifndef C4MLIB_MACRO_STD_TYPE_H
#define C4MLIB_MACRO_STD_TYPE_H

#include <stddef.h>
#include <stdint.h>

/* Public Section Start */
/**
 * @brief 無回傳、參數為(void *)型態之函式型態。
 */
typedef void (*Func_t)(void *);

/**
 * @brief 回傳char、參數為(void *) 之函式型態。
 */
typedef uint8_t (*TaskFunc_t)(void *);
/* Public Section End */

#endif  // C4MLIB_MACRO_STD_TYPE_H

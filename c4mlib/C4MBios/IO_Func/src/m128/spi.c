/**
 * @file spi.c
 * @author LiYu87
 * @date 2019.07.15
 * @author ya058764
 * @date 2021.06.05
 * @brief spi 暫存器操作相關函式實作。
 */

#include "c4mlib/C4MBios/IO_Func/src/spi.h"

#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"

#include <avr/io.h>

uint8_t SPI_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                uint8_t Data) {
    if (REG_p != &SPCR && REG_p != &SPSR && REG_p != &DDRB && REG_p != &DDRF) {
        return RES_ERROR_LSBYTE;
    }

    REGFPT(REG_p, Mask, Shift, Data);
    return RES_OK;
}

uint8_t SPI_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                void *Data_p) {
    if (REG_p != &SPCR && REG_p != &SPSR && REG_p != &DDRB && REG_p != &DDRF) {
        return RES_ERROR_LSBYTE;
    }

    REGFGT(REG_p, Mask, Shift, Data_p);
    return RES_OK;
}

uint8_t SPI_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (REG_p == &SPDR) {
        if (Bytes == 1) {
            SPDR = *((uint8_t *)Data_p);
        }
        else {
            return RES_ERROR_BYTES;
        }
    }
    else {
        return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

uint8_t SPI_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (REG_p == &SPDR) {
        if (Bytes == 1) {
            *((uint8_t *)Data_p) = SPDR;
        }
        else {
            return RES_ERROR_BYTES;
        }
    }
    else {
        return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

/**
 * @file adc.c
 * @author LiYu87
 * @date 2019.07.15
 * @brief adc 暫存器操作函式各硬體實作分割。
 */

#if defined(__AVR_ATmega128__)
#    include "m128/adc.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/adc.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/adc.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif

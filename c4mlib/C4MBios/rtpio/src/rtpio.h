/**
 * @file rtpio.h
 * @author Yi-Mou
 * @brief RealTimePort函式原型
 * @date 2019-08-16
 *
 *
 */

#ifndef C4MLIB_RTPIO_RTPIO_H
#define C4MLIB_RTPIO_RTPIO_H

#include <stdint.h>
/* Public Section Start */

/**
 * @defgroup rtpio_macro
 * @defgroup rtpio_func
 * @defgroup rtpio_struct
 */

/* Public Section Start */
#define RTPIO_MAX_DATA_LENGTH                                                  \
    2  ///<硬體暫存器位元最大大小  @ingroup rtpio_macro

/**
 * @brief RealTimeRegIO結構原型
 *
 * @ingroup rtpio_struct
 *
 * 存取硬體暫存器位址和其大小後續讀寫用。
 * 資料緩衝暫存區則供資料存放，當執行step函式時將資料讀寫暫存區。
 * 計數值則紀錄執行step函式次數，用以追蹤次數。
 */
typedef struct {
    volatile uint8_t* Reg_p;
    uint8_t Bytes;
    uint8_t Task_Id;
    volatile uint8_t* uIn_p;
    volatile uint8_t* yOut_p;
    uint8_t ExCount;
    volatile uint8_t y;
    uint8_t* ExCountOut_p;
    uint8_t NextTaskNum;
    uint8_t** NextTask_esp;
} RealTimeRegIOStr_t;

/**
 * @brief RealTimeFlag結構原型
 *
 * @ingroup rtpio_struct
 *
 * 存取硬體暫存器位址和其大小後續讀寫用。
 * 資料緩衝暫存區則供資料存放，當執行step函式時將旗標資料讀寫暫存區。
 * 計數值則紀錄執行step函式次數，用以追蹤次數。
 */
typedef struct {
    volatile uint8_t* Reg_p;
    uint8_t Task_Id;
    uint8_t Mask;
    uint8_t Shift;
    volatile uint8_t* uIn_p;
    volatile uint8_t* yOut_p;
    uint8_t ExCount;
    volatile uint8_t y;
    uint8_t* ExCountOut_p;
    uint8_t NextTaskNum;
    uint8_t* NextTask_p;
} RealTimeFlagIOStr_t;

/**
 * @brief 執行一次暫存器讀取，並將觸發計數+1。
 *
 * @ingroup rtpio_func
 * @param VoidStr_p 要執行的結構指標。
 *
 * 執行硬體暫存器輸入埠之讀取並轉存至資料結構內的資料暫存區，並將觸發次數值加1。配合其資料結構實體，可登錄在中斷服務常式中執行。
 */
void RealTimeRegGet_step(void* VoidStr_p);

/**
 * @brief 執行一次暫存器寫入，並將觸發計數+1。
 *
 * @ingroup rtpio_func
 * @param VoidStr_p 要執行的結構指標。
 *
 * 執行資料結構內的資料暫存區之讀取並轉存至硬體暫存器輸出埠，並將觸發次數值加1。配合其資料結構實體，可登錄在中斷服務常式中執行。
 */
void RealTimeRegPut_step(void* VoidStr_p);

/**
 * @brief 執行一次旗標讀取，並將觸發計數+1。
 *
 * @ingroup rtpio_func
 * @param VoidStr_p 要執行的結構指標。
 *
 * 執行硬體暫存器輸入埠之讀取並轉存至資料結構內的資料暫存區，並將觸發次數值加1。配合其資料結構實體，可登錄在中斷服務常式中執行。
 */
void RealTimeFlagGet_step(void* VoidStr_p);

/**
 * @brief 執行一次旗標寫入，並將觸發計數+1。
 *
 * @ingroup rtpio_func
 * @param VoidStr_p 要執行的結構指標。
 *
 * 執行資料結構內的資料暫存區之讀取並轉存至硬體暫存器輸出埠，
 * 並將觸發次數值加1。配合其資料結構實體，可登錄在中斷服務常式中執行。
 */
void RealTimeFlagPut_step(void* VoidStr_p);

#define RT_REG_IO_LAY(RTRIOSTR, NEXTTASKNUM, HWREGADD, BYTES, DATA_P)          \
    RealTimeRegIOStr_t RTRIOSTR = {.Task_Id      = 0,                          \
                                   .ExCount      = 0,                          \
                                   .NextTaskNum  = NEXTTASKNUM,                \
                                   .Reg_p        = HWREGADD,                   \
                                   .Bytes        = BYTES,                      \
                                   .yOut_p       = &(RTRIOSTR.y),              \
                                   .ExCountOut_p = &(RTRIOSTR.ExCount)}

#define RT_FLAG_IO_LAY(RTFIOSTR, NEXTTASKNUM, HWREGADD, MASK, SHIFT, DATA_P)   \
    RealTimeFlagIOStr_t RTFIOSTR = {.Task_Id      = 0,                         \
                                    .ExCount      = 0,                         \
                                    .NextTaskNum  = NEXTTASKNUM,               \
                                    .Reg_p        = HWREGADD,                  \
                                    .Mask         = MASK,                      \
                                    .Shift        = SHIFT,                     \
                                    .yOut_p       = &(RTFIOSTR.y),             \
                                    .ExCountOut_p = &(RTFIOSTR.ExCount)}
/* Public Section End */

#endif  // C4MLIB_RTPIO_RTPIO_H

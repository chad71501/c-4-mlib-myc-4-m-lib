/**
 * @file rtpio.c
 * @author Yi-Mou
 * @date 2019.08.16
 * @brief RealTimePort實現
 *
 */

#include "rtpio.h"

#include "c4mlib/C4MBios/macro/src/bits_op.h"

#include <stddef.h>

#define OK          0
#define ERROR_BYTES 1

void RealTimeRegGet_step(void* VoidStr_p) {
    RealTimeRegIOStr_t* Str_p = (RealTimeRegIOStr_t*)VoidStr_p;
    REGGET(Str_p->Reg_p, Str_p->Bytes, Str_p->yOut_p);
    Str_p->ExCount++;

    // TODO: 
    /* pipeline task trigger*/
    /*
        TRIG_NEXT_TASK(Str_p->NextTaskNum);
    
    */
}

void RealTimeRegPut_step(void* VoidStr_p) {
    RealTimeRegIOStr_t* Str_p = (RealTimeRegIOStr_t*)VoidStr_p;
    REGPUT(Str_p->Reg_p, Str_p->Bytes, Str_p->uIn_p);
    Str_p->ExCount++;

    // TODO: 
    /* pipeline task trigger*/
    /*
        TRIG_NEXT_TASK(Str_p->NextTaskNum);
    
    */
}

void RealTimeFlagGet_step(void* VoidStr_p) {
    RealTimeFlagIOStr_t* Str_p = (RealTimeFlagIOStr_t*)VoidStr_p;
    REGFGT(Str_p->Reg_p, Str_p->Mask, Str_p->Shift, Str_p->yOut_p);
    Str_p->ExCount++;

    // TODO: 
    /* pipeline task trigger*/
    /*
        TRIG_NEXT_TASK(Str_p->NextTaskNum);
    
    */
}

void RealTimeFlagPut_step(void* VoidStr_p) {
    RealTimeFlagIOStr_t* Str_p = (RealTimeFlagIOStr_t*)VoidStr_p;
    REGFPT(Str_p->Reg_p, Str_p->Mask, Str_p->Shift, *Str_p->uIn_p);
    Str_p->ExCount++;

    // TODO: 
    /* pipeline task trigger*/
    /*
        TRIG_NEXT_TASK(Str_p->NextTaskNum);
    
    */
}

/**
 * @file test_rtupcount.c
 * @author Yi-Mou
 * @brief realtimeupcount實作
 * @date 2019-08-26
 *
 * 紀錄timer中斷發生次數，計算出秒數
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/rtpio/src/rtupcount.h"

#include <avr/io.h>


RT_ISRCOUNT_LAY(RealTimeUpCount_1,0);

void init_timer2();  // timer2 CTC模式 波型頻率:100Hz

int main() {
    C4M_DEVICE_set();

    init_timer2();
    sei();

    uint8_t times = 0;
    DDRA = 0xFF;
    while (1) {
        if (RealTimeUpCount_1.ExCount == 200) {
            times++;
            RealTimeUpCount_1.ExCount = 0;
        }
        printf("%d\n", times);
    }
}

void init_timer2() {
    OCR2 = 53;
    TCCR2 = 0b000001101;
    TCNT2 = 0;
    TIMSK |= 1 << OCIE2;
}
ISR(TIMER2_COMP_vect){
    RealTimeISRCount_step(&RealTimeUpCount_1);
}

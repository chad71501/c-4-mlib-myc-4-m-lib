C4MLIB 檔案架構
***************

| 主寫者： `LiYu87 <https://gitlab.com/mickey9910326>`_
| 編輯者：
| 日期： 2019.01.25

1. 底層資料夾
=============

| c4mlib 底層資料夾結構：

.. code-block:: text

    c4mlib
      │  makefile        // c4mlib 專案建置用 makefile
      │  README.md       // c4mlib 專案簡介
      │  .editorconfig   // c4mlib 專案檔案格式及編碼規定
      │  .gitignore      // c4mlib 專案git忽略清單
      │
      ├─c4mlib   // 存放各模組專案資料夾
      ├─scripts  // 存放各種腳本ex:專案建置腳本、文檔建置腳本
      ├─dist     // distribution 存放專案建置好的目標檔，以利發佈
      └─docs     // 存放專案文檔

| 這些命名都是大家的通識，雖然沒有硬性的規定，可是大部分的專案資料夾結構及命名都
| 會差不多，可以參考下方的stackoverflow問題頁面。
| `stackoverflow - What is the meaning of the /dist directory in open source projects? <https://stackoverflow.com/questions/22842691/what-is-the-meaning-of-the-dist-directory-in-open-source-projects>`_


2. c4mlib/c4mlib 資料夾
=======================

| 功能簡介：存放各模組專案資料夾。

| c4mlib/c4mlib 是由各種模組組成的，例如：hardware模組、hmi模組、asabus模組。
| c4mlib/c4mlib 底下的每一個資料夾皆會對應到一個模組，而該資料夾我們稱作
| 「 **模組專案資料夾** 」。

| 每個模組專案資料夾都是一個獨立的專案，有自己的目標檔、編譯方式、測試檔，
| 標準的模組專案資料夾結構如下：

.. code-block:: text

    c4mlib/c4mlib/hmi
      │  makefile            // 模組專案的makefile
      │
      ├─src                  // 存放原始碼的資料夾
      │      hmi.c           // 原始碼1
      │      hmi.h           // 原始碼2
      │
      ├─test                   // 存放測試檔的資料夾
      │      test_put_array.c  // 測試1
      │      test_put_struct.c // 測試2
      │
      └─unittest               // 存放自動測試檔的資料夾
             unittest_hmi.c    // 自動測試1

| 最終c4mlib/c4mlib資料夾底下的結構會長這樣：

.. code-block:: text

    c4mlib/c4mlib
      │  gcc.mk     // 設定gcc參數及編譯各檔案(.o,.h)方式的makefile
      │  makefile   // 建置c4mlib.a的makefile
      │
      ├─common      // 模組專案資料夾1
      ├─hardware    // 模組專案資料夾2
      ├─hmi         // 模組專案資料夾3
      ├─asabus      // 模組專案資料夾4
      ├─7s00        // 模組專案資料夾5
      └─kb00        // 模組專案資料夾6

3. c4mlib/scripts 資料夾
========================

| 功能簡介：存放各種腳本。

.. code-block:: text

    c4mlib/scripts
         build.py      // 建置c4mlib的腳本
         settings.ini  // 腳本的設定檔
         gendoc.py     // 建置c4mlib文檔的腳本

4. c4mlib/dist 資料夾
=====================

| 功能簡介：存放專案目標檔。

| 專案建置好後，會包含要發佈給他人使用的c4mlib壓縮檔，並有各種平台之版本，以及
| c4mlib的文檔。

.. code-block:: text

    c4mlib/dist
      ├─c4mlib_0.1.0_m128.zip
      ├─c4mlib_0.1.0_m88.zip
      ├─c4mlib_0.1.0_tiny2313.zip
      └─c4mlib_0.1.0_manual.pdf

5. c4mlib/docs 資料夾
=====================

| 功能簡介：存放專案文檔。

| c4mlib 的文檔是使用一連串的工具建置起來的，包含 doxygen、sphinx、breathe、
| readthedocs、MiKTeX。詳見章節「 **c4mlib 文檔建置及修改方式** 」。

# c4mlib 的 中斷處理

## 目的


為了讓使用者可以在 runtime 時期去註冊中斷，函式庫開發者需要先行
註冊中斷，以開發像 tim_isr() 的函示。
但使用者就無法透過 avrlibc 中的 ISR 去註冊中斷向量了。

為了解決此問題，使用了取多 weak function 去實現這功能。<br/>
新增了一個 macro INTERNAL_ISR，以及重新定義了 avrlibc 中的 ISR 。

修改之後，<br/>
**函式庫開發者**可以使用 **INTERNAL_ISR** 去註冊中斷向量。<br/>
而**函式庫使用者**可使用 **ISR** 去註冊中斷向量，並覆蓋掉**函式庫開發者**先行註冊的。<br/>

### 函式宣告及實現：
實現方法如下兩段程式碼所示：

``` c
/*
    Function vectXX() is used for avr-gcc link intterupt.
    Function __vectXX_routine()  is used for lib developing.
    Function __vectXX_routine1() is used for normal user.
    Both __vectXX_routine and __vectXX_routine1 is weak type, can be overwritted
    in other files by use ISR and INTERNAL_ISR.
*/

/* External Interrupt Request 0 */
void __INT0_vect_routine1() {
    ;
}
void INT0_vect_routine1() __attribute__((weak, alias("__INT0_vect_routine1")));
void __INT0_vect_routine() {
    INT0_vect_routine1();
};
void INT0_vect_routine() __attribute__((weak, alias("__INT0_vect_routine")));
void INT0_vect (void) __attribute__ ((signal,__INTR_ATTRS));
void INT0_vect (void) {
    INT0_vect_routine();
}
```

給函式庫使用者使用的 macro ISR ：

``` c
// for normal user, it use just like ISR in avrlibc

/* Public Section Start */
#include <avr/interrupt.h>
#undef ISR
#ifdef __cplusplus
#  define ISR(vector, ...)            \
    extern "C" void vector ## _routine (void) __attribute__ ((__INTR_ATTRS)) __VA_ARGS__; \
    void vector ## _routine (void)
#else
#  define ISR(vector, ...)            \
    void vector ## _routine (void) __attribute__ ((__INTR_ATTRS)) __VA_ARGS__; \
    void vector ## _routine (void)
#endif
/* Public Section End */

```

給函式庫開發者使用的 macro INTERNAL_ISR：

``` c
// for lib internal developing
#ifdef __cplusplus
#  define INTERNAL_ISR(vector, ...)            \
    extern "C" void vector ## _routine1 (void) __VA_ARGS__; \
    void vector ## _routine1 (void)
#else
#  define INTERNAL_ISR(vector, ...)            \
    void vector ## _routine1 (void) __VA_ARGS__; \
    void vector ## _routine1 (void)
#endif
```

- - -

### 說明：

以下說明皆用 INT0_vect 去做範例說明

- - -

#### 1. 各中斷向量的 routine1

``` c
void __INT0_vect_routine1() {
    ;
}
void INT0_vect_routine1() __attribute__((weak, alias("__INT0_vect_routine1")));
```

這邊宣告了 INT0_vect_routine1 並有兩個 attribute。<br/>
其一為 `alias("__INT0_vect_routine1")` ， 此 attribute 會跟 linker 講說這個
symbol 是 \_\_INT0_vect_routine1 的 alias ， 所以 INT0_vect_routine1 的內容會等於
\_\_INT0_vect_routine1 。<br/>
其一為 `weak` ，此 attribute 會跟 linker 講說這個 symbol 還沒被實作，
所以可以在其他地方宣告這個 symbol ，並覆蓋此 symbol 的內容。

如此一來我們可以在其他地方實作 INT0_vect_routine1 ，去取代預設的內容。<br/>
而我們又設計了 macro `INTERNAL_ISR` 去執行上述這件事情，也就是實作個中斷向量的 routine1 函式。

``` c
#  define INTERNAL_ISR(vector, ...)            \
    void vector ## _routine1 (void) __VA_ARGS__; \
    void vector ## _routine1 (void)
```

Ex : **函式庫開發者**要使用計時器3的overflow中斷。可以透過 **INTERNAL_ISR** 去註冊。

``` c
INTERNAL_ISR(TIMER3_OVF_vect) {
    do something ...
}
```

如此一來在 precompile 後，TIMER3_OVF_vect 取代 macro 中的
vector ，最後這段程式碼會變成：

``` c
void TIMER3_OVF_vect_routine1 (void) {
    do something ...
}
```

- - -

#### 2. 各中斷向量的 routine

```
void __INT0_vect_routine() {
    INT0_vect_routine1();
};
void INT0_vect_routine() __attribute__((weak, alias("__INT0_vect_routine")));
```
這邊大致上與前段相同，差別在 routine 之後沒有 1。而取代的 macro 變成 ISR 。  


```
#  define ISR(vector, ...)            \
    void vector ## _routine (void) __attribute__ ((__INTR_ATTRS)) __VA_ARGS__; \
    void vector ## _routine (void)
```

Ex : **函式庫使用者**要使用計時器3的overflow中斷。可以透過 **ISR** 去註冊。註冊後就可以取代**函式庫開發者**註冊好的。

``` c
ISR(TIMER3_OVF_vect) {
    do something ...
}
```

- - -

#### 3. 連結到各中斷向量

``` c
void INT0_vect (void) __attribute__ ((signal,__INTR_ATTRS));
void INT0_vect (void) {
    INT0_vect_routine();
}
```

INT0_vect 是編譯器預設的中斷向量函式，並將前述的 INT0_vect_routine 放入其中，連結到中斷向量中。<br/>
如此一來 INT0_vect 的內容是呼叫 INT0_vect_routine 。<br/>
**INT0_vect_routine** 是 \_\_INT0_vect_routine 的 weak alias。<br/>
\_\_INT0_vect_routine 的內容是呼叫 INT0_vect_routine1 。<br/>
**INT0_vect_routine1** 是 \_\_INT0_vect_routine1 的 weak alias。<br/>
\_\_INT0_vect_routine 的內容是空的 。    

- - -

使用層級的示意圖如下：<br/>
![](../_static/dev-note-interrupt-1.png)

- - -

## Symbol 問題
ref:  
[How to force gcc to link unreferenced, static C++ objects from a library](https://stackoverflow.com/questions/4767925/how-to-force-gcc-to-link-unreferenced-static-c-objects-from-a-library)

在 linker 時期因只會在 archive 搜索已使用之 symbol ，所以目前中斷實現方式無法透過正常方式連結。

需要加入 linker 參數 -Wl,--whole-archive 去搜索整個 archive 中的 symbol
，才可以正常把各中斷向量(\_\_vector\_N)連結到isr函式。並在結束搜索時加入 linker
參數 -Wl,--no-whole-archive 關閉搜索所有 symbol，避免 linker 連結到 avr-libc 中 archive 重複的 symbol ，導致編譯失敗。

ex:  
``` shell
avr-gcc -mmcu=atmega128 test_timer.o \
    -Wl,--whole-archive ../dist/libc4m.a \
    -Wl,--no-whole-archive \
    -o test_timer.elf
```

use -Wl,-u,__INT0_vect_routine

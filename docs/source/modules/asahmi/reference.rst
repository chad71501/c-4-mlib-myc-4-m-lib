Reference
*********

函式介面
========

.. doxygengroup:: asahmi_func
   :project: c4mlib
   :content-only:

巨集 Macros
===========

.. doxygengroup:: asahmi_macro
   :project: c4mlib
   :content-only:

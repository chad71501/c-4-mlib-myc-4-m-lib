.. c4mlib documentation master file, created by
   sphinx-quickstart on Mon Dec  3 15:55:43 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to C4MLIB's documentation!
===================================
   
.. toctree::
   :caption: 模組列表
   :maxdepth: 1

   modules/index

.. toctree::
   :caption: 開發人員須知
   :maxdepth: 1

   develop/index

.. toctree::
   :caption: 開發筆記
   :maxdepth: 1

   dev-note/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


